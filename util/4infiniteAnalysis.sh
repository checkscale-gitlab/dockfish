#!/bin/bash
#to install:
#cd to dockfish directory
#mvn install
#cd web/target (or better: copy dockfish-web-runner.jar to an working directory

export engine_directory=c:\\Prog\\chessengines
export rabbitmq_host=REPLACE
export rabbitmq_port=5672
export rabbitmq_username=REPLACE
export rabbitmq_password=REPLACE
export "queue_name_tasks=notebookjobs"
export "routing_key_task=windows,longterm"
#export "routing_key_task=netcup"

export "fallback_post_url=REPLACE"

export uci_option_Threads=4
export uci_option_Hash=8092
export uci_option_Contempt=0
export uci_option_SyzygyPath=c:\\Prog\\sy3-4-5

java -Dquarkus.http.port=8072 -jar dockfish-web-runner.jar

#and then move to locahost:8072

