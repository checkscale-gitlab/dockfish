package ce.chess.commontech.resource.jaxrs;

import ce.util.log.BuildInfo;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Path("/api/ping")
public class PingResource {

  private static final Logger LOGGER = LogManager.getLogger(PingResource.class);

  @Inject
  BuildInfo buildInfo;

  @GET
  public Response get() {
    String versionString = buildInfo.getAsString();
    LOGGER.info(versionString);
    return Response.ok(versionString).build();
  }
}
