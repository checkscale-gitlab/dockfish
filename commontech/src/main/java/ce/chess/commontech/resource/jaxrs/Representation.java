package ce.chess.commontech.resource.jaxrs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Target({ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Value.Style(
    allParameters = true,
    defaultAsDefault = true,
    depluralize = true,
    jdkOnly = true,
    passAnnotations = {Schema.class},
    visibility = Value.Style.ImplementationVisibility.PUBLIC
)
public @interface Representation {
}
