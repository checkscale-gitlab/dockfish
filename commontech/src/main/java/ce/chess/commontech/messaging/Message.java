package ce.chess.commontech.messaging;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.MessageProperties;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class Message {

  private static final Charset DEFAULT_MESSAGE_CHARSET = StandardCharsets.UTF_8;
  private static final int DELIVERY_MODE_PERSISTENT = 2;
  private static final String REDELIVER_HEADER = "REDELIVER_HEADER";

  private byte[] bodyContent = new byte[0];
  private BasicProperties basicProperties;
  private String exchange = "";
  private String routingKey = "";

  public Message() {
    this(MessageProperties.BASIC);
  }

  public Message(BasicProperties basicProperties) {
    this.basicProperties = basicProperties;
  }

  public BasicProperties getBasicProperties() {
    return basicProperties;
  }

  public byte[] getBody() {
    return Arrays.copyOf(bodyContent, bodyContent.length);
  }

  public String getBodyAsString() {
    return readBodyAsString();
  }

  public String getExchange() {
    return this.exchange;
  }

  public String getRoutingKey() {
    return this.routingKey;
  }

  public Message withExchange(String exchange) {
    this.exchange = exchange;
    return this;
  }

  public Message withRoutingKey(String routingKey) {
    this.routingKey = routingKey;
    return this;
  }

  public Message withBody(byte[] bodyContent) {
    this.bodyContent = Arrays.copyOf(bodyContent, bodyContent.length);
    return this;
  }

  public Message withBody(String body) {
    fillBodyFrom(body);
    return this;
  }

  public Message withContentType(String contentType) {
    basicProperties = basicProperties.builder()
        .contentType(contentType)
        .build();
    return this;
  }


  public Message asPersistent() {
    basicProperties = basicProperties.builder()
        .deliveryMode(DELIVERY_MODE_PERSISTENT)
        .build();
    return this;
  }

  private String readBodyAsString() {
    Charset charset = Optional.ofNullable(getBasicProperties())
        .map(BasicProperties::getContentEncoding)
        .map(Charset::forName)
        .orElse(DEFAULT_MESSAGE_CHARSET);
    return new String(bodyContent, charset);
  }


  private void fillBodyFrom(String messageText) {
    basicProperties = basicProperties.builder()
        .contentEncoding(DEFAULT_MESSAGE_CHARSET.name())
        .build();
    byte[] bodyBytes = messageText.getBytes(DEFAULT_MESSAGE_CHARSET);
    withBody(bodyBytes);
  }

  public void setRedelivery() {
    Map<String, Object> headers = Optional.ofNullable(basicProperties.getHeaders())
            .map(ConcurrentHashMap::new)
            .orElseGet(ConcurrentHashMap::new);
    Integer newRedeliveries = (Integer) headers.getOrDefault(REDELIVER_HEADER, 0) + 1;
    headers.put(REDELIVER_HEADER, newRedeliveries);
    basicProperties = basicProperties.builder()
        .headers(headers)
        .build();
  }

  public boolean isRedelivered() {
    return isRedelivered(1);
  }

  public boolean isRedelivered(int maxCount) {
    Map<String, Object> headers = Optional.ofNullable(basicProperties.getHeaders())
        .orElseGet(Map::of);
    return (Integer) headers.getOrDefault(REDELIVER_HEADER, 0) >= maxCount;
  }
}
