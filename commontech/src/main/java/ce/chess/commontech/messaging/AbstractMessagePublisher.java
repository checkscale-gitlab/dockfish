package ce.chess.commontech.messaging;

import ce.chess.commontech.messaging.rabbit.RabbitSender;

import com.rabbitmq.client.BuiltinExchangeType;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

public abstract class AbstractMessagePublisher {
  private static final String EMPTY_STRING = "";

  @Inject
  protected RabbitSender rabbitSender;

  public abstract String getExchangeName();

  public BuiltinExchangeType getExchangeType() {
    return BuiltinExchangeType.FANOUT;
  }

  public String getQueueName() {
    return EMPTY_STRING;
  }

  public String getRoutingKeySending() {
    return EMPTY_STRING;
  }

  public List<String> getRoutingKeysBinding() {
    return Collections.singletonList(EMPTY_STRING);
  }

  public Map<String, Object> getQueueArguments() {
    return Collections.emptyMap();
  }

  protected void publish(String payload) throws IOException {
    publish(payload, this.getRoutingKeySending());
  }

  protected void publish(String payload, String routingKey) throws IOException {
    Message message = createMessage(payload, routingKey);
    rabbitSender.publish(message);
  }

  protected void publishAndClose(String payload) throws IOException {
    publishAndClose(payload, this.getRoutingKeySending());
  }

  protected void publishAndClose(String payload, String routingKey) throws IOException {
    Message message = createMessage(payload, routingKey);
    try (RabbitSender closeableSender = rabbitSender) {
      closeableSender.publish(message);
    }
  }

  private Message createMessage(String payload, String routingKey) {
    return new Message()
        .withExchange(getExchangeName())
        .withRoutingKey(routingKey)
        .withContentType("application/json")
        .asPersistent()
        .withBody(payload);
  }

}
