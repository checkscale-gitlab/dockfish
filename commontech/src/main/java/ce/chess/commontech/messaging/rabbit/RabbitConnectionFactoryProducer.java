package ce.chess.commontech.messaging.rabbit;

import static java.lang.Boolean.TRUE;

import com.rabbitmq.client.impl.ForgivingExceptionHandler;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class RabbitConnectionFactoryProducer {

  private static final String RABBITMQ_HOST_PROPERTY = "rabbitmq_host";
  private static final String RABBITMQ_PORT_PROPERTY = "rabbitmq_port";
  private static final String RABBITMQ_USERNAME_PROPERTY = "rabbitmq_username";
  private static final String RABBITMQ_SECRET_PROPERTY = "rabbitmq_password";
  private static final String RABBITMQ_VIRTUALHOST_PROPERTY = "rabbitmq_virtualhost";
  private static final String RABBITMQ_USE_SSL_PROPERTY = "rabbitmq_use_ssl";

  private SingleConnectionFactory connectionFactory;

  @ConfigProperty(name = RABBITMQ_HOST_PROPERTY)
  Optional<String> host = Optional.empty();

  @ConfigProperty(name = RABBITMQ_PORT_PROPERTY)
  Optional<Integer> port = Optional.empty();

  @ConfigProperty(name = RABBITMQ_USERNAME_PROPERTY)
  Optional<String> username = Optional.empty();

  @ConfigProperty(name = RABBITMQ_SECRET_PROPERTY)
  Optional<String> password = Optional.empty();

  @ConfigProperty(name = RABBITMQ_VIRTUALHOST_PROPERTY)
  Optional<String> virtualhost = Optional.empty();

  @ConfigProperty(name = RABBITMQ_USE_SSL_PROPERTY)
  Optional<Boolean> useSsl = Optional.empty();

  @Produces
  public SingleConnectionFactory produceConnectionFactory() {
    if (connectionFactory == null) {
      connectionFactory = createConnectionFactory();
    }
    return connectionFactory;
  }

  @Produces
  public RabbitSender produceRabbitSender() {
    return new RabbitSender(produceConnectionFactory());
  }

  private SingleConnectionFactory createConnectionFactory() {
    SingleConnectionFactory newConnectionFactory = new SingleConnectionFactory();
    host.ifPresent(newConnectionFactory::setHost);
    port.ifPresent(newConnectionFactory::setPort);
    virtualhost.ifPresent(newConnectionFactory::setVirtualHost);
    username.ifPresent(newConnectionFactory::setUsername);
    password.ifPresent(newConnectionFactory::setPassword);
    useSsl.filter(TRUE::equals).ifPresent(x -> {
      try {
        newConnectionFactory.useSslProtocol();
      } catch (NoSuchAlgorithmException | KeyManagementException ex) {
        throw new IllegalStateException(ex);
      }
    });
    newConnectionFactory.setExceptionHandler(new ForgivingExceptionHandler());
    return newConnectionFactory;
  }

}
