package ce.util.log;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.Properties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PropertyFileLoaderTest {

  private PropertyFileLoader cut;

  @BeforeEach
  void setup() {
    cut = new PropertyFileLoader();
  }

  @Test
  void findsDefaultProperties() throws IOException {
    Properties properties = cut.loadProperties(BuildInfo.BUILD_INFO_FILE_NAME);

    System.out.println(properties);
    assertThat(properties.getProperty("Revision"), is(not(blankOrNullString())));
  }

  @Test
  void readsBuildProperties() throws IOException {
    Properties properties = cut.loadProperties("buildinfoForTest.properties");

    assertThat(properties.getProperty("Revision"), is(equalTo("revision1")));
    assertThat(properties.getProperty("Commit"), is(equalTo("commit1")));
    assertThat(properties.getProperty("BuildDate"), is(equalTo("date1")));

  }

  @Test
  void failsOnNonExistingProperties() {
    assertThrows(RuntimeException.class, () -> cut.loadProperties("doesNotExist"));
  }

  @Test
  void returnsEmptyProperties() throws IOException {
    Properties properties = cut.loadProperties("empty.properties");

    assertThat(properties.stringPropertyNames(), hasSize(0));
  }
}
