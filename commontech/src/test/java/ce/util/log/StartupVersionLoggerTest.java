package ce.util.log;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StartupVersionLoggerTest {
  @Mock
  private BuildInfo buildInfo;

  @InjectMocks
  private StartupVersionLogger cut;

  @Test
  void contextInitializedDeterminesVersionString() {
    given(buildInfo.getAsString()).willReturn("anyVersion");

    cut.contextInitialized(null);

    verify(buildInfo).getAsString();
  }
}
