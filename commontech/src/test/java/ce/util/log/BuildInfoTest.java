package ce.util.log;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BuildInfoTest {

  @Mock
  PropertyFileLoader propertyFileLoader;

  @InjectMocks
  BuildInfo cut;

  @Test
  void findsBuildInfoPropertiesInResourcePath() throws IOException {
    given(propertyFileLoader.loadProperties(any())).willCallRealMethod();

    String buildInfo = cut.getAsString();

    System.out.println("buildInfo = " + buildInfo);
    assertThat(buildInfo, is(not(nullValue())));
  }

  @Test
  void yieldsDefaultTextOnError() throws IOException {
    given(propertyFileLoader.loadProperties(any()))
        .willThrow(new IOException("thrown by " + this.getClass().getName()));

    String buildInfo = cut.getAsString();

    System.out.println("buildInfo = " + buildInfo);
    assertThat(buildInfo, is(not(nullValue())));
  }

}
