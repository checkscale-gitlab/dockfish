package ce.chess.commontech.resource.jaxrs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ce.util.log.BuildInfo;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
class PingResourceTest {

  @InjectMock
  private BuildInfo buildInfo;

  @BeforeEach
  void setUp() {
    when(buildInfo.getAsString()).thenReturn("Mocked BuildInfo");
  }

  @Test
  void shouldReturn200OkAndBuildInfo() {
    given()
        .when().get("/api/ping")
        .then()
        .statusCode(200)
        .body(containsString("Mocked BuildInfo"));
    verify(buildInfo).getAsString();
  }

}
