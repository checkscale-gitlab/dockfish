package ce.chess.commontech.messaging.rabbit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.rabbitmq.client.Connection;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SingleConnectionFactoryTest {

  @Mock
  private Connection connection;

  @InjectMocks
  private SingleConnectionFactory singleConnectionsFactory;

  @Spy
  private SingleConnectionFactory cut;

  private Connection newCreatedConnection = mock(Connection.class);

  @BeforeEach
  void setup() {
    cut = spy(singleConnectionsFactory);
  }

  @Test
  void doesCreateNewConnectionWhenNull() throws IOException, TimeoutException {
    doReturn(newCreatedConnection).when(cut).newConnection(any(), anyList());
    connection = null;

    Connection result = cut.newConnection();

    assertThat(result, is(equalTo(newCreatedConnection)));
  }

  @Test
  void doesCreateNewConnectionWhenNotOpen() throws IOException, TimeoutException {
    doReturn(newCreatedConnection).when(cut).newConnection(any(), anyList());
    given(connection.isOpen()).willReturn(false);

    Connection result = cut.newConnection();

    assertThat(result, is(equalTo(newCreatedConnection)));
  }

  @Test
  void doesReuseOpenConnection() throws IOException, TimeoutException {
    given(connection.isOpen()).willReturn(true);

    Connection result = cut.newConnection();

    assertThat(result, is(equalTo(connection)));
    assertThat(result, is(not(equalTo(newCreatedConnection))));
  }

}
