package ce.chess.commontech.messaging.rabbit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.impl.ForgivingExceptionHandler;
import io.quarkus.test.junit.QuarkusTest;
import javax.inject.Inject;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@QuarkusTest
class RabbitConnectionFactoryProducerTest {
  @Inject
  SingleConnectionFactory connectionFactory;

  @Inject
  RabbitSender rabbitSender;

  @Nested
  class GivenConfigurationIsPresent {
    @Test
    void producesRabbitSender() {
      assertThat(rabbitSender, is(notNullValue()));
    }

    @Test
    void producesConnectionFactory() {
      assertThat(connectionFactory, is(notNullValue()));
    }

    @Test
    void producedConnectionFactoryIsASingleConnectionFactory() {
      assertThat(connectionFactory, is(instanceOf(SingleConnectionFactory.class)));
    }

    @Test
    void returnsConnectionsFactoryFromConfiguration() {
      assertThat(connectionFactory.getHost(), is(equalTo("testhost")));
      assertThat(connectionFactory.getPort(), is(equalTo(4272)));
      assertThat(connectionFactory.getUsername(), is(equalTo("bugs")));
      assertThat(connectionFactory.getPassword(), is(equalTo("bunny")));
      assertThat(connectionFactory.getVirtualHost(), is(equalTo("vh")));
      assertThat(connectionFactory.isSSL(), is(true));
    }

    @Test
    void returnsConnectionFactoryWithForgivingExceptionHandler() {
      assertThat(connectionFactory.getExceptionHandler().getClass(), is(ForgivingExceptionHandler.class));
    }
  }

  @Nested
  class GivenConfigurationIsNotPresent {

    private RabbitConnectionFactoryProducer cutMocked = new RabbitConnectionFactoryProducer();

    @Test
    void thenReturnDefaultValues() {
      ConnectionFactory connectionFactory = cutMocked.produceConnectionFactory();

      assertThat(connectionFactory.getHost(), is(equalTo("localhost")));
      assertThat(connectionFactory.getPort(), is(equalTo(5672)));
      assertThat(connectionFactory.getUsername(), is(equalTo("guest")));
      assertThat(connectionFactory.getPassword(), is(equalTo("guest")));
      assertThat(connectionFactory.getVirtualHost(), is(equalTo("/")));
      assertThat(connectionFactory.isSSL(), is(false));
    }
  }
}
