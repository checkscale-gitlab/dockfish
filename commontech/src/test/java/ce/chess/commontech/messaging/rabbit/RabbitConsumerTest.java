package ce.chess.commontech.messaging.rabbit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ce.chess.commontech.messaging.AbstractMessageListener;
import ce.chess.commontech.messaging.Message;
import ce.chess.commontech.messaging.RequeueException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.io.UncheckedIOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RabbitConsumerTest {
  @Mock
  private AbstractMessageListener listener;

  @Mock
  private Channel channel;

  @Mock
  private Envelope envelope;

  @Captor
  private ArgumentCaptor<Message> messageCaptor;

  @InjectMocks
  private RabbitConsumer cut;

  private BasicProperties basicProperties;

  @BeforeEach
  void setUp() {
    basicProperties = MessageProperties.BASIC;
  }

  @Test
  void isARabbitConsumer() {
    assertThat(cut, is(instanceOf(Consumer.class)));
  }

  @Test
  void createsMessageAndDelegateToListener() throws IOException {
    given(envelope.getExchange()).willReturn("exchange");
    given(envelope.getRoutingKey()).willReturn("routing");

    cut.handleDelivery("consumerTag", envelope, basicProperties, new byte[]{83, 105, 77, 65});

    verify(listener).handleMessage(messageCaptor.capture());
    Message message = messageCaptor.getValue();
    assertThat(message.getBasicProperties(), is(basicProperties));
    assertThat(message.getExchange(), is(equalTo("exchange")));
    assertThat(message.getRoutingKey(), is(equalTo("routing")));
    assertThat(message.getBodyAsString(), is(equalTo("SiMA")));
  }

  @Nested
  class WhenAutoAcknowledgeIsOff {
    @BeforeEach
    void setup() {
      given(listener.isAutoAck()).willReturn(false);
    }

    @Nested
    class AndListenerThrowsAnError {
      @BeforeEach
      void setup() {
        doThrow(UncheckedIOException.class).when(listener).handleMessage(any());
      }

      @Test
      void thenNackTheMessage() throws IOException {
        given(envelope.getDeliveryTag()).willReturn(42L);

        cut.handleDelivery("consumerTag", envelope, basicProperties, new byte[]{83, 105, 77, 65});

        verify(channel).basicNack(42L, false, false);
      }
    }

    @Nested
    class AndListenerThrowsAnRequeueError {
      @BeforeEach
      void setup() {
        doThrow(RequeueException.class).when(listener).handleMessage(any());
      }

      @Test
      void thenNackAndRepublish() throws IOException {
        given(envelope.getDeliveryTag()).willReturn(42L);
        given(envelope.getExchange()).willReturn("exchange");
        given(envelope.getRoutingKey()).willReturn("routingKey");
        given(listener.getMaxRedeliveryAttempts()).willReturn(1);

        byte[] body = {83, 105, 77, 65};
        cut.handleDelivery("consumerTag", envelope, basicProperties, body);

        verify(channel).basicNack(42L, false, false);
        verify(channel).basicPublish(eq("exchange"), eq("routingKey"), any(), eq(body));
      }
    }

    @Nested
    class AndListenerSucceeds {
      @Test
      void thenAckTheMessage() throws IOException {
        given(envelope.getDeliveryTag()).willReturn(42L);

        cut.handleDelivery("consumerTag", envelope, basicProperties, new byte[]{83, 105, 77, 65});

        verify(channel).basicAck(42L, false);
      }

      @Test
      void whenAckFailsThenRethrow() throws IOException {
        given(envelope.getDeliveryTag()).willReturn(42L);
        doThrow(IOException.class).when(channel).basicAck(42L, false);

        assertThrows(IOException.class, () ->
            cut.handleDelivery("consumerTag", envelope, basicProperties, new byte[]{83, 105, 77, 65}));
      }

    }
  }

  @Nested
  class WhenAutoAcknowledgeIsOn {
    @BeforeEach
    void setup() {
      given(listener.isAutoAck()).willReturn(true);
    }

    @Test
    void thenDoNotAcknowledgeAfterSuccess() throws IOException {
      cut.handleDelivery("consumerTag", envelope, basicProperties, new byte[]{83, 105, 77, 65});

      verify(channel, never()).basicAck(anyLong(), anyBoolean());
      verify(channel, never()).basicNack(anyLong(), anyBoolean(), anyBoolean());
    }

    @Test
    void thenDoNotNackAfterError() throws IOException {
      doThrow(UncheckedIOException.class).when(listener).handleMessage(any());

      cut.handleDelivery("consumerTag", envelope, basicProperties, new byte[]{83, 105, 77, 65});

      verify(channel, never()).basicAck(anyLong(), anyBoolean());
      verify(channel, never()).basicNack(anyLong(), anyBoolean(), anyBoolean());
    }
  }
}
