package ce.chess.commontech.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ce.chess.commontech.messaging.rabbit.RabbitSender;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import java.io.IOException;
import java.util.Collections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AbstractMessagePublisherTest {

  private static final int DELIVERYMODE_PERSISTENT = 2;

  @Mock
  private RabbitSender rabbitSender;

  @Captor
  private ArgumentCaptor<Message> messageCaptor;

  @InjectMocks
  private AbstractMessagePublisher cut = new AbstractMessagePublisher() {
    @Override
    public String getExchangeName() {
      return "anyExchangeName";
    }
  };

  @Nested
  class WhenAStringIsPublished {
    private String content = "This is content";

    @BeforeEach
    void publishString() throws IOException {
      cut.publish(content);
    }

    @Test
    void thenSendAMessageWithContent() throws IOException {
      verify(rabbitSender).publish(messageCaptor.capture());
      Message message = messageCaptor.getValue();
      assertThat(message.getBodyAsString(), is(equalTo(content)));
    }

    @Test
    void thenSendAMessageWithGivenRouting() throws IOException {
      verify(rabbitSender).publish(messageCaptor.capture());
      Message message = messageCaptor.getValue();
      assertThat(message.getExchange(), is(equalTo(cut.getExchangeName())));
      assertThat(message.getRoutingKey(), is(equalTo(cut.getRoutingKeySending())));
    }

    @Test
    void thenSendAMessageWithDefaultProperties() throws IOException {
      verify(rabbitSender).publish(messageCaptor.capture());
      Message message = messageCaptor.getValue();
      AMQP.BasicProperties basicProperties = message.getBasicProperties();
      assertThat(basicProperties.getContentType(), is(equalTo("application/json")));
      assertThat(basicProperties.getDeliveryMode(), is(equalTo(DELIVERYMODE_PERSISTENT)));
    }

    @Test
    void thenTheSenderIsNotClosed() throws IOException {
      verify(rabbitSender).publish(any());
      verify(rabbitSender, never()).close();
    }
  }

  @Nested
  class WhenAStringIsPublishedToClosingSender {
    private String content = "This is content";

    @BeforeEach
    void publishStringAndClose() throws IOException {
      cut.publishAndClose(content);
    }

    @Test
    void thenTheSenderIsClosedAfterPublish() throws IOException {
      InOrder inOrder = inOrder(rabbitSender);
      inOrder.verify(rabbitSender).publish(any());
      inOrder.verify(rabbitSender).close();
    }
  }

  @Test
  void exchangeTypeDefaultsToFanOut() {
    assertThat(cut.getExchangeType(), is(equalTo(BuiltinExchangeType.FANOUT)));
  }

  @Test
  void doesReturnEmptyRoutingKeysBinding() {
    assertThat(cut.getRoutingKeysBinding(), contains(""));
  }

  @Test
  void doesReturnEmptyRoutingKeySending() {
    assertThat(cut.getRoutingKeySending(), is(equalTo("")));
  }

  @Test
  void doesReturnEmptyQueueName() {
    assertThat(cut.getQueueName(), is(equalTo("")));
  }

  @Test
  void doesReturnEmptyQueueArguments() {
    assertThat(cut.getQueueArguments(), is(equalTo(Collections.emptyMap())));
  }
}
