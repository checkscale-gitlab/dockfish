package ce.chess.commontech.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.mock;

import com.google.common.primitives.Bytes;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.MessageProperties;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class MessageTest {

  private static final byte[] AE_BYTES_UTF8 = new byte[]{(byte) 0xC3, (byte) 0xA4};
  private static final byte[] AE_BYTES_UTF16 = new byte[]{(byte) 0x00, (byte) 0xE4};
  private static final byte[] messageBytes = new byte[]{83, 105, 77, 65};
  private static final String REDELIVER_HEADER = "REDELIVER_HEADER";

  private Message cut;


  @BeforeEach
  void setup() {
    cut = new Message();
  }

  @Test
  void doesGetAndSetBodyFromBytes() {
    assertThat(cut.getBodyAsString(), is(emptyOrNullString()));
    assertThat(Bytes.asList(cut.getBody()), hasSize(0));

    cut.withBody(messageBytes);

    assertThat(cut.getBodyAsString(), is(equalTo("SiMA")));
    cut.getBodyAsString();
    assertThat(Bytes.asList(cut.getBody()), contains(Byte.valueOf("83"), Byte.valueOf("105"),
        Byte.valueOf("77"), Byte.valueOf("65")));
  }

  @Test
  void doesGetAndSetBodyFromString() {
    assertThat(cut.getBodyAsString(), is(emptyOrNullString()));
    assertThat(Bytes.asList(cut.getBody()), hasSize(0));

    cut.withBody("SiMA");

    assertThat(cut.getBodyAsString(), is(equalTo("SiMA")));
    assertThat(Bytes.asList(cut.getBody()), contains(Byte.valueOf("83"), Byte.valueOf("105"),
        Byte.valueOf("77"), Byte.valueOf("65")));
  }

  @Nested
  class WhenMessageBodyIsFilledFromStringValue {
    @Test
    void thenTheContentEncodingWillBeSetToUtf8() {
      cut.withBody("äöü");

      assertThat(cut.getBasicProperties().getContentEncoding(), is(equalTo("UTF-8")));
    }

    @Test
    void thenTheUtf8EncodingWillBeApplied() {
      cut.withBody("äöü");

      assertThat(cut.getBodyAsString(), is(equalTo("äöü")));
    }
  }

  @Nested
  class WhenMessageBodyIsReturnedAsString {
    @Nested
    class AndContentEncodingWasExplicitlyGiven {
      @Test
      void thenTheGivenContentEncodingWillBeApplied() {
        cut = new Message(MessageProperties.BASIC.builder().contentEncoding(StandardCharsets.UTF_16.name()).build());

        cut.withBody(AE_BYTES_UTF16);
        assertThat(cut.getBodyAsString(), is(equalTo("ä")));

        cut.withBody(AE_BYTES_UTF8);
        assertThat(cut.getBodyAsString(), is(not(equalTo("ä"))));
      }
    }

    @Nested
    class AndContentEncodingWasIsNotExplicitelyDefined {
      @Test
      void thenTheDefaultUtf8EncodingWillBeApplied() {
        cut.withBody(AE_BYTES_UTF16);
        assertThat(cut.getBodyAsString(), is(not(equalTo("ä"))));

        cut.withBody(AE_BYTES_UTF8);
        assertThat(cut.getBodyAsString(), is(equalTo("ä")));
      }
    }
  }

  @Test
  void doesGetAndSetExchangeName() {
    assertThat(cut.getExchange(), is(emptyOrNullString()));

    cut = cut.withExchange("anyExchange");

    assertThat(cut.getExchange(), is(equalTo("anyExchange")));
  }

  @Test
  void doesGetAndSetRoutingKey() {
    assertThat(cut.getRoutingKey(), is(emptyOrNullString()));

    cut = cut.withRoutingKey("anyRoutingKey");

    assertThat(cut.getRoutingKey(), is(equalTo("anyRoutingKey")));
  }

  @Test
  void doesSetContentTypeInBasicProperties() {
    assertThat(cut.getBasicProperties().getContentType(), is(not("anyContentType")));

    cut = cut.withContentType("anyContentType");

    assertThat(cut.getBasicProperties().getContentType(), is(equalTo("anyContentType")));
  }

  @Test
  void doesSetPersistentFlagInBasicProperties() {
    assertThat(cut.getBasicProperties().getDeliveryMode(), is(not(2)));

    cut = cut.asPersistent();

    assertThat(cut.getBasicProperties().getDeliveryMode(), is(2));
  }

  @Test
  void isCreatedWithBasicMessageProperties() {
    assertThat(cut.getBasicProperties(), is(equalTo(MessageProperties.BASIC)));
  }

  @Test
  void canBeCreatedWithAlternativeMessageProperties() {
    AMQP.BasicProperties mockProperties = mock(AMQP.BasicProperties.class);

    Message newMessage = new Message(mockProperties);

    assertThat(newMessage.getBasicProperties(), is(equalTo(mockProperties)));
  }

  @Test
  void setsRedeliveries() {
    assertThat(cut.isRedelivered(), is(false));

    cut.setRedelivery();

    assertThat(cut.getBasicProperties().getHeaders().get(REDELIVER_HEADER), is(equalTo(1)));
    assertThat(cut.isRedelivered(), is(true));
  }
}
