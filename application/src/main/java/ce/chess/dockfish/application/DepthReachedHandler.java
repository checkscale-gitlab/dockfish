package ce.chess.dockfish.application;

import ce.chess.dockfish.domain.event.DepthReached;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.EvaluationRepository;
import ce.chess.dockfish.domain.model.result.Variation;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.TaskRepository;

import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Singleton
public class DepthReachedHandler {
  private static final Logger sLogger = LogManager.getLogger(DepthReachedHandler.class);

  private final EvaluationRepository evaluationRepository;
  private final TaskRepository taskRepository;
  private final AdaptPvService adaptPvService;

  @Inject
  DepthReachedHandler(EvaluationRepository evaluationRepository, TaskRepository taskRepository,
                      AdaptPvService adaptPvService) {
    this.evaluationRepository = Objects.requireNonNull(evaluationRepository);
    this.taskRepository = Objects.requireNonNull(taskRepository);
    this.adaptPvService = Objects.requireNonNull(adaptPvService);
  }

  public void newDepthReached(@Observes DepthReached event) {
    evaluationRepository.findByTaskIdMaxCreated(event.taskId()).ifPresentOrElse(
        evaluation -> taskRepository.findByTaskId(event.taskId()).ifPresentOrElse(
            task -> {
              log(evaluation, task);
              adaptPvService.adaptPv(evaluation, task);
            },
            () -> sLogger.warn("Task not found")),
        () -> sLogger.warn("Evaluation not found"));
  }

  private void log(Evaluation evaluation, EngineAnalysisRequest task) {
    String variationLog =
        evaluation.variations().stream().map(Variation::shortRepresentation).collect(Collectors.joining());
    sLogger.info("d{}{},{},{},{},{} {}",
        evaluation::maxDepth,
        () -> variationLog,
        () -> evaluation.uciState().shortRepresentation(),
        () -> task.name().orElse(""),
        task::engineProgramName,
        evaluation::analysisTime,
        () -> task.estimatedCompletionTime()
            .map(t -> "finishes at " + DateTimeFormatter.ISO_LOCAL_TIME.format(t)).orElse("")
    );
  }
}
