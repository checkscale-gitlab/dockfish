package ce.chess.dockfish.application;

import ce.chess.commontech.messaging.RequeueException;
import ce.chess.dockfish.application.command.EngineOption;
import ce.chess.dockfish.application.command.SubmitTaskCommand;
import ce.chess.dockfish.application.command.UciOptionsConfiguration;
import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.result.ResultGame;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableEngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.domain.model.task.TaskRepository;
import ce.chess.dockfish.infrastructure.engine.EngineController;
import ce.chess.dockfish.infrastructure.engine.EngineListener;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
public class InfiniteAnalysisService {
  private static final Logger sLogger = LogManager.getLogger(InfiniteAnalysisService.class);

  @Inject
  EngineController engineController;

  @Inject
  EngineListener engineListener;

  @Inject
  TaskRepository taskRepository;

  @Inject
  UciOptionsConfiguration uciOptionsConfiguration;

  @Inject
  Config config;

  public Optional<TaskId> startAsync(SubmitTaskCommand command) {
    // Use this for rest calls.
    // It will return with a task id immediately and block further analysis requests until finished.
    sLogger.info("Start asynchronous analysis");
    if (!engineController.tryAcquireLock()) {
      sLogger.info("Task rejected. Engine is still running");
      return Optional.empty();
    }
    return startAnalysis(command);
  }

  public Optional<TaskId> startSync(SubmitTaskCommand command) {
    // Use this for messaging.
    // It will block until calculation is finished.
    sLogger.info("Start synchronous analysis");
    if (isTaskAlreadyAnalysed(command)) {
      sLogger.info("Task rejected immediately. Has already been analysed.");
      return Optional.empty();
    }
    engineController.acquireLock();
    Optional<TaskId> taskId = startAnalysis(command);
    taskId.ifPresent(t -> engineController.blockWhileActive());
    return taskId;
  }

  private Optional<TaskId> startAnalysis(SubmitTaskCommand command) {
    try {
      TaskId taskId = TaskId.createNew();
      EngineAnalysisRequest engineTask = createEngineTask(command, taskId);
      if (taskRepository.hasDuplicate(engineTask)) {
        sLogger.info("Task rejected. Has already been analysed.");
        engineController.releaseLock();
        return Optional.empty();
      }
      engineTask = startController(engineTask);
      taskRepository.save(engineTask);
      return Optional.of(taskId);
    } catch (Exception ex) { // NOPMD
      engineController.releaseLock();
      throw ex;
    }
  }

  private EngineAnalysisRequest startController(EngineAnalysisRequest engineTask) {
    try {
      return engineController.startAnalysis(engineTask, engineListener);
    } catch (Exception ex) { // NOPMD
      throw new RequeueException(ex.getMessage(), ex);
    }
  }

  public JobStatus getJobStatus(TaskId taskId) {
    if (engineController.uciEngineIsRunning(taskId)) {
      return JobStatus.ACTIVE;
    } else {
      return JobStatus.NOT_ACTIVE;
    }
  }

  public boolean stopAnalysis() {
    sLogger.info("Stop analysis");
    engineController.stop();
    return !engineController.uciEngineIsRunning();
  }

  public EngineAnalysisRequest getTaskDetails(TaskId taskId) {
    return taskRepository.findByTaskId(taskId).orElseThrow(IllegalArgumentException::new);
  }

  private boolean isTaskAlreadyAnalysed(SubmitTaskCommand command) {
    EngineAnalysisRequest engineTask = createEngineTask(command, TaskId.createNew());
    return taskRepository.hasDuplicate(engineTask);
  }

  private EngineAnalysisRequest createEngineTask(SubmitTaskCommand command, TaskId taskId) {
    ResultGame resultGame = ResultGame.fromPgn(command.pgn());
    return ImmutableEngineAnalysisRequest.builder()
        .taskId(taskId)
        .name(command.name())
        .engineProgramName(command.engineId())
        .hostname(config.getOptionalValue("hostname", String.class).orElse("hostname"))
        .startingPosition(resultGame)
        .startingMoveNumber(resultGame.getLastMovePly())
        .initialPv(command.initialPv())
        .maxDepth(command.maxDepth())
        .maxDuration(command.maxDuration())
        .engineOptions(mergeLocalUciOptions(command).options())
        .useSyzygyPath(command.useSyzygyPath())
        .dynamicPv(command.dynamicPv())
        .created(LocalDateTime.now(ZoneId.systemDefault()))
        .build();
  }

  private SubmitTaskCommand mergeLocalUciOptions(SubmitTaskCommand command) {
    SubmitTaskCommand result = command;
    for (EngineOption option : uciOptionsConfiguration.getLocalEngineOptions()) {
      if ("SyzygyPath".equals(option.name())) {
        if (command.useSyzygyPath()) {
          result = result.addOrReplaceOption(option);
        }
      } else {
        result = result.addOrReplaceOption(option);
      }
    }
    return result;
  }
}
