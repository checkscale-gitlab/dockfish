package ce.chess.dockfish.application;

import ce.chess.dockfish.domain.model.staticevaluation.ImmutableStaticEvaluation;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluation;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluationRequest;
import ce.chess.dockfish.infrastructure.engine.EngineController;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@ApplicationScoped
public class StaticEvaluationService {

  @Inject
  EngineController engineController;

  @Inject
  Event<StaticEvaluation> resultPublisher;

  public void createAndPublishEvaluation(StaticEvaluationRequest request) {
    engineController.acquireLock();
    try {
      String evaluation = engineController.retrieveStaticEvaluation(request.fen());
      resultPublisher.fire(ImmutableStaticEvaluation.of(request, evaluation));
    } finally {
      engineController.releaseLock();
    }
  }

}
