package ce.chess.dockfish.application.command;

import ce.chess.commontech.resource.jaxrs.Representation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableSubmitTaskCommand.class)
@JsonDeserialize(as = ImmutableSubmitTaskCommand.class)
@Schema(name = "SubmitTaskCommand")
public interface SubmitTaskCommand {

  @Schema(example = "name")
  Optional<String> name();

  @Schema(example = "1.d4 d5 2.c4 *")
  String pgn();

  @Schema(example = "4")
  Integer initialPv();

  @Schema(example = "40")
  Optional<Integer> maxDepth();

  @Schema(type = SchemaType.STRING,
      example = "PT2H",
      description = "ISO-8601 string, see"
          + "https://docs.oracle.com/javase/8/docs/api/java/time/Duration.html#parse-java.lang.CharSequence-")
  Optional<Duration> maxDuration();

  @Schema
  List<EngineOption> options();

  @Schema
  Optional<DynamicPv> dynamicPv();

  @Schema(nullable = true, defaultValue = "stockfish")
  default String engineId() {
    return "stockfish";
  }

  @Schema(nullable = true, defaultValue = "false")
  default boolean useSyzygyPath() {
    return false;
  }

  @Value.Check
  default void eitherDurationOrDepthMustBePresent() {
    Preconditions.checkArgument(maxDuration().isPresent() || maxDepth().isPresent(),
        "Either Depth or Duration must be given");
  }

  default SubmitTaskCommand addOrReplaceOption(EngineOption newOption) {
    List<EngineOption> withDroppedOption = options().stream()
        .filter(o -> !o.name().equalsIgnoreCase(newOption.name()))
        .collect(Collectors.toList());
    withDroppedOption.add(newOption);
    return ImmutableSubmitTaskCommand.copyOf(this).withOptions(withDroppedOption);

  }

}
