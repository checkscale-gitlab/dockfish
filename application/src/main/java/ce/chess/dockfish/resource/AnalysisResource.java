package ce.chess.dockfish.resource;

import ce.chess.dockfish.application.EvaluationMessageService;
import ce.chess.dockfish.application.InfiniteAnalysisService;
import ce.chess.dockfish.application.command.SubmitTaskCommand;
import ce.chess.dockfish.domain.model.result.ImmutableEvaluationMessage;
import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.infrastructure.engine.EngineDirectoryConfiguration;

import java.net.URI;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

@Path("/api")
public class AnalysisResource {
  private static final String TASK_ID_MUST_BE_GIVEN_MSG = "taskId must be given";

  @Context
  private UriInfo uriInfo;

  @Inject
  InfiniteAnalysisService analysisService;

  @Inject
  EvaluationMessageService evaluationMessageService;

  @Inject
  EngineDirectoryConfiguration engineDirectoryConfiguration;

  @GET
  @Path("/engines")
  @Produces(MediaType.APPLICATION_JSON)
  @Operation(summary = "Get a list auf available engines", description = "Returns a list of engine names")
  public Response getEngineNames() {
    return Response.ok()
        .entity(
            engineDirectoryConfiguration.listEngineNames().stream()
                .sorted()
                .toList())
        .build();
  }

  @GET
  @Path("/tasks")
  @Produces(MediaType.APPLICATION_JSON)
  @Operation(summary = "Get information about all tasks", description = "Returns a list of Tasks")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "The task list",
          content = @Content(schema = @Schema(type = SchemaType.ARRAY,
              implementation = ImmutableTaskRepresentation.class))),
      @APIResponse(responseCode = "204", description = "No Content")})
  public Response getTaskList() {
    List<TaskId> taskIds = evaluationMessageService.getAllTaskIds();
    if (taskIds.isEmpty()) {
      return Response.noContent().entity("Found no task at all").build();
    } else {
      List<TaskRepresentation> elements = taskIds.stream()
          .map(this::createTaskRepresentation)
          .sorted(Comparator.comparing(TaskRepresentation::submitted).reversed())
          .toList();
      return Response.ok().entity(elements).build();
    }
  }

  @GET
  @Path("/tasks/{taskId}")
  @Produces(MediaType.APPLICATION_JSON)
  @Operation(summary = "Get the evaluation of the given TaskId.",
      description = "Returns the evaluation. TaskId may be set to \"current\"")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "The current/last analysis result",
          content = @Content(schema = @Schema(implementation = ImmutableEvaluationMessage.class))),
      @APIResponse(responseCode = "400", description = "Mandatory Request parameter is missing"),
      @APIResponse(responseCode = "404", description = "Not Found (unknown TaskId)")})
  public Response get(@PathParam("taskId") String rawTaskId) {
    if (rawTaskId.isEmpty()) {
      return Response.status(Response.Status.BAD_REQUEST).entity(TASK_ID_MUST_BE_GIVEN_MSG).build();
    }
    return evaluationResponse(rawTaskId);
  }

  @GET
  @Path("/tasks/{taskId}/all")
  @Produces(MediaType.APPLICATION_JSON)
  @Operation(summary = "Get all evaluations for the given TaskId",
      description = "Returns all evaluations. TaskId may be set to \"current\"")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "The current/last analysis result",
          content = @Content(schema = @Schema(type = SchemaType.ARRAY,
              implementation = ImmutableEvaluationMessage.class))),
      @APIResponse(responseCode = "400", description = "Mandatory Request parameter is missing"),
      @APIResponse(responseCode = "404", description = "Not Found (unknown TaskId)")})
  public Response getAll(@PathParam("taskId") String rawTaskId) {
    if (rawTaskId.isEmpty()) {
      return Response.status(Response.Status.BAD_REQUEST).entity(TASK_ID_MUST_BE_GIVEN_MSG).build();
    }
    return allEvaluationsResponse(rawTaskId);
  }

  @POST
  @Path("/tasks")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Operation(summary = "Post an analysis task",
      description = "Returns the task representation of the posted Task")
  @APIResponses(value = {
      @APIResponse(responseCode = "202", description = "The task representation",
          content = @Content(schema = @Schema(implementation = ImmutableTaskRepresentation.class))),
      @APIResponse(responseCode = "400", description = "Invalid format"),
      @APIResponse(responseCode = "409", description = "The server is already running an analysis")})
  public Response submitTask(SubmitTaskCommand command) {
    Optional<TaskId> newTaskId = analysisService.startAsync(command);
    return newTaskId.map(this::taskSubmittedResponse)
        .orElseGet(() -> Response.status(Status.CONFLICT).entity("Already pondering. Post did not succeed.").build());
  }

  @GET
  @Path("/tasks/stop")
  @Produces(MediaType.APPLICATION_JSON)
  @Operation(summary = "Stop the current analysis",
      description = "Returns the last evaluation")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "The current/last analysis result",
          content = @Content(schema = @Schema(implementation = ImmutableEvaluationMessage.class))),
      @APIResponse(responseCode = "204", description = "No active analysis")})
  public Response stop() {
    boolean stopped = analysisService.stopAnalysis();
    if (stopped) {
      return evaluationMessageService.getLastEvaluationMessage()
          .map(Response::ok).map(Response.ResponseBuilder::build)
          .orElseGet(() -> Response.noContent().build());
    }
    return Response.noContent().build();
  }

  @POST
  @Path("/tasks/{taskId}/stop")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
  @Operation(summary = "Stop the given task",
      description = "Returns the last evaluation")
  @APIResponses(value = {
      @APIResponse(responseCode = "200", description = "The current task has been stopped",
          content = @Content(schema = @Schema(implementation = ImmutableEvaluationMessage.class))),
      @APIResponse(responseCode = "400", description = "Mandatory Request parameter is missing")})
  public Response stop(@PathParam("taskId") String rawTaskId) {
    if (rawTaskId.isEmpty()) {
      return Response.status(Status.BAD_REQUEST).entity(TASK_ID_MUST_BE_GIVEN_MSG).build();
    }
    analysisService.stopAnalysis();
    return evaluationResponse(rawTaskId);
  }

  private Response taskSubmittedResponse(TaskId taskId) {
    TaskRepresentation representation = createTaskRepresentation(taskId);
    URI uri = createGetTaskLink(taskId.rawId());
    return Response.status(Status.ACCEPTED)
        .location(uri)
        .contentLocation(uri)
        .entity(representation)
        .build();
  }

  private Response evaluationResponse(String taskId) {
    if ("current".equals(taskId)) {
      return evaluationMessageService.getLastEvaluationMessage()
          .map(Response::ok)
          .orElseGet(Response::noContent)
          .build();
    } else {
      return evaluationMessageService.getLastEvaluationMessage(ImmutableTaskId.of(taskId))
          .map(Response::ok)
          .orElseGet(Response::noContent)
          .build();
    }
  }

  private Response allEvaluationsResponse(String taskId) {
    if ("current".equals(taskId)) {
      return Response
          .ok(evaluationMessageService.getAllEvaluations())
          .build();
    } else {
      return Response
          .ok(evaluationMessageService.getAllEvaluations(ImmutableTaskId.of(taskId)))
          .build();
    }
  }

  private TaskRepresentation createTaskRepresentation(TaskId taskId) {
    JobStatus jobStatus = analysisService.getJobStatus(taskId);
    EngineAnalysisRequest taskDetails = analysisService.getTaskDetails(taskId);
    URI uri = createGetTaskLink(taskId.rawId());
    return ImmutableTaskRepresentation.builder()
        .taskId(taskId.rawId())
        .taskName(taskDetails.name().orElse(""))
        .submitted(taskDetails.created())
        .startingPosition(taskDetails.startingPosition().pgn())
        .startingMoveNumber(taskDetails.startingMoveNumber())
        .engineProgramName(taskDetails.engineProgramName())
        .hostname(taskDetails.hostname())
        .initialPv(taskDetails.initialPv())
        .maxDepth(taskDetails.maxDepth())
        .maxDuration(taskDetails.maxDuration())
        .useSyzygyPath(taskDetails.useSyzygyPath())
        .estimatedCompletionTime(taskDetails.estimatedCompletionTime())
        .status(jobStatus)
        .link(uri.toString())
        .build();
  }

  private URI createGetTaskLink(String rawTaskId) {
    return uriInfo.getBaseUriBuilder()
        .path(AnalysisResource.class).path(AnalysisResource.class, "get")
        .build(rawTaskId);
  }

}
