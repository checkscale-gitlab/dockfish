package ce.chess.dockfish.resource;

import ce.chess.commontech.resource.jaxrs.Representation;
import ce.chess.dockfish.domain.model.result.JobStatus;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableTaskRepresentation.class)
@JsonDeserialize(as = ImmutableTaskRepresentation.class)
@Schema(name = "TaskRepresentation")
public interface TaskRepresentation {
  String taskId();

  String taskName();

  LocalDateTime submitted();

  String startingPosition();

  Integer startingMoveNumber();

  String engineProgramName();

  String hostname();

  Integer initialPv();

  Optional<Integer> maxDepth();

  Optional<Duration> maxDuration();

  boolean useSyzygyPath();

  Optional<LocalDateTime> estimatedCompletionTime();

  JobStatus status();

  String link();

}
