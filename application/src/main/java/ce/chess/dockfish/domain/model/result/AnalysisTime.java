package ce.chess.dockfish.domain.model.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import org.immutables.value.Value;
import org.immutables.value.Value.Style.ImplementationVisibility;

@Value.Immutable
@Value.Style(visibility = ImplementationVisibility.PRIVATE)
@JsonSerialize
public interface AnalysisTime {

  static AnalysisTime fromMilliSeconds(long millis) {
    return new AnalysisTimeBuilder().value(Duration.ofMillis(millis)).build();
  }

  static AnalysisTime fromMinutes(int minutes) {
    return new AnalysisTimeBuilder().value(Duration.ofMinutes(minutes)).build();
  }

  @JsonValue
  default String formattedAsTime() {
    return LocalTime.MIDNIGHT.plus(value()).format(DateTimeFormatter.ofPattern("HH:mm:ss"));
  }

  @Value.Parameter
  @JsonIgnore
  Duration value();

}
