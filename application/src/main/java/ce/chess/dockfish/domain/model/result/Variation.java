package ce.chess.dockfish.domain.model.result;

import ce.chess.commontech.resource.jaxrs.Representation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableVariation.class)
@JsonDeserialize(as = ImmutableVariation.class)
@Schema(name = "Variation")
public interface Variation {
  int pvId();

  String moves();

  Score score();

  int depth();

  AnalysisTime time();

  String pgn();

  @Value.Derived
  @JsonIgnore
  default String shortRepresentation() {
    return "[p" + pvId() + '=' + firstMove() + '/' + score().toString() + ']';
  }

  @Value.Derived
  @JsonIgnore
  default String firstMove() {
    return firstMove(moves());
  }

  static String firstMove(String moveList) {
    boolean numberFound = false;
    boolean moveFound = false;
    StringBuilder builder = new StringBuilder();
    for (String part : moveList.split(" ")) {
      if (!numberFound && Character.isDigit(part.charAt(0))) {
        builder.append(part).append(' ');
        numberFound = true;
      }
      if ("...".equals(part)) {
        builder.append(part).append(' ');
      }
      if (!moveFound && Character.isAlphabetic(part.charAt(0))) {
        builder.append(part);
        moveFound = true;
      }
    }
    return builder.toString();
  }

}
