package ce.chess.dockfish.domain.model.result;

import ce.chess.commontech.resource.jaxrs.Representation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Set;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableUciState.class)
@JsonDeserialize(as = ImmutableUciState.class)
@Schema(name = "UciState")
public interface UciState {
  long kiloNodes();

  long kiloNodesPerSecond();

  long tbHits();

  Set<String> infoStrings();

  @Value.Derived
  @JsonIgnore
  default String shortRepresentation() {
    return "[Uci: " + kiloNodes() + "kN, " + kiloNodesPerSecond() + "kN/s, " + tbHits() + "tbHits]";
  }
}
