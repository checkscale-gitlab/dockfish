package ce.chess.dockfish.domain.model.result;

import ce.chess.commontech.resource.jaxrs.Representation;
import ce.chess.dockfish.domain.model.task.TaskId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableEvaluation.class)
@JsonDeserialize(as = ImmutableEvaluation.class)
@Schema(name = "Evaluation")
public interface Evaluation {

  TaskId taskId();

  LocalDateTime created();

  @Schema(type = SchemaType.ARRAY, implementation = ImmutableVariation.class)
  List<Variation> variations();

  @Schema(implementation = ImmutableUciState.class)
  UciState uciState();

  @Value.Derived
  @JsonIgnore
  default int sizeOfCurrentVariations() {
    return currentVariations().size();
  }

  @Value.Derived
  @JsonIgnore
  default List<Variation> currentVariations() {
    int maxDepth = maxDepth();
    return variations().stream()
        .filter(v -> v.depth() > maxDepth - 2)
        .toList();
  }

  @Value.Derived
  @JsonIgnore
  default boolean hasAllVariationsOfSameDepth() {
    return allVariationsHavingSameDepth().test(this);
  }

  @Value.Derived
  @JsonIgnore
  default int maxDepth() {
    return variations().stream().mapToInt(Variation::depth).max().orElse(0);
  }

  @Value.Derived
  @JsonIgnore
  default String analysisTime() {
    return variations().isEmpty() ? "00:00" : variations().get(0).time().formattedAsTime();
  }

  @Value.Derived
  default int determineNumberOfGoodPv(int cutoffScore) {
    return determineNumberOfGoodPv(variations(), cutoffScore);
  }

  static int determineNumberOfGoodPv(List<Variation> variations, int cutoffScore) {
    boolean isWhitesMove = ResultGame.fromPgn(variations.get(0).pgn()).isWhitesMove();
    int bestScoreOfWhite = variations.stream()
        .mapToInt(v -> v.score().centiPawns())
        .max().orElse(0);
    int bestScoreOfBlack = variations.stream()
        .mapToInt(v -> v.score().centiPawns())
        .min().orElse(0);

    if (isWhitesMove) {
      return (int) variations.stream()
          .mapToInt(v -> v.score().centiPawns())
          .filter(s -> s >= bestScoreOfWhite - cutoffScore)
          .count();
    } else {
      return (int) variations.stream()
          .mapToInt(v -> v.score().centiPawns())
          .filter(s -> s <= bestScoreOfBlack + cutoffScore)
          .count();
    }
  }

  static Predicate<Evaluation> matching(TaskId taskId) {
    return e -> e.taskId().matches(taskId);
  }

  static Predicate<Evaluation> allVariationsHavingSameDepth() {
    return e -> e.currentVariations().stream()
        .map(Variation::depth)
        .distinct()
        .limit(2)
        .count() == 1;
  }

  @JsonIgnore
  @Value.Derived
  default String taskIdAndMaxDepth() {
    return taskId().rawId() + maxDepth();
  }

  @Value.Derived
  @JsonIgnore
  default String shortForm() {
    Variation bestVariation = currentVariations().get(0);
    return "d=" + bestVariation.depth() + ": " + bestVariation.firstMove() + " " + bestVariation.score().toString();

  }
}
