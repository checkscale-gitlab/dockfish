package ce.chess.dockfish.domain.model.result;

import static java.util.function.Predicate.not;

import com.google.common.base.Strings;
import org.immutables.value.Value;
import org.immutables.value.Value.Style.ImplementationVisibility;
import raptor.chess.Game;
import raptor.chess.GameFactory;
import raptor.chess.pgn.ListMaintainingPgnParserListener;
import raptor.chess.pgn.PgnParser;
import raptor.chess.pgn.PgnParserError;
import raptor.chess.pgn.SimplePgnParser;

@Value.Immutable
@Value.Style(visibility = ImplementationVisibility.PRIVATE)
public abstract class ResultGame {

  public static ResultGame fromPgn(String pgn) {
    return fromGame(gameFromPgn(pgn));
  }

  public static ResultGame fromGame(Game game) {
    return new ResultGameBuilder().pgn(game.toPgn()).build();
  }

  @Value.Parameter
  public abstract String pgn();

  public Game asGame() {
    // must always be calculated freshly from string because of mutable MoveList in Game - no @Value.Derived!
    return gameFromPgn(pgn());
  }

  public int getLastMovePly() {
    return asGame().getHalfMoveCount();
  }

  public boolean isWhitesMove() {
    return asGame().isWhitesMove();
  }

  public String fen() {
    return asGame().toFen();
  }


  public String notation() {
    return pgn().replaceAll("(?m)^\\[.*(?:\\r?\\n)?", "").replaceAll("\\r\\n|\\r|\\n", " ").trim();
  }

  private static Game gameFromPgn(String pgnIn) {
    String pgnString = fixPgnString(pgnIn);

    PgnParser parser = new SimplePgnParser(pgnString);
    ListMaintainingPgnParserListener listener = new ListMaintainingPgnParserListener();
    parser.addPgnParserListener(listener);

    parser.parse();

    if (errorsOccurred(listener)) {
      throw new IllegalArgumentException("Invalid pgn: " + listener.getErrors());
    }

    Game result;
    switch (listener.getGames().size()) {
      case 0:
        result = GameFactory.createStartingPosition();
        break;
      case 1:
        result = listener.getGames().get(0);
        break;
      default:
        throw new IllegalArgumentException("Expected valid pgn for exactly one game, but found "
            + listener.getGames().size());
    }
    result.addState(Game.UPDATING_SAN_STATE);
    return result;
  }

  private static boolean errorsOccurred(ListMaintainingPgnParserListener listener) {
    return listener.getErrors().stream()
        .map(PgnParserError::getType)
        .anyMatch(not(PgnParserError.Type.UNEXPECTED_GAME_END::equals));
  }

  private static String fixPgnString(String pgnString) {
    if (Strings.isNullOrEmpty(pgnString)) {
      throw new IllegalArgumentException("Input PGN must not be empty");
    }
    String pgnOut = pgnString.trim().replaceAll("(\\b)\\*", " *");
    if (!pgnOut.startsWith("[")) {
      pgnOut = "[Event \" \"]\r\n" + pgnOut;
    }
    if (!pgnOut.endsWith("*\n")) {
      pgnOut = pgnOut + " *\n";
    }
    return pgnOut;
  }

}
