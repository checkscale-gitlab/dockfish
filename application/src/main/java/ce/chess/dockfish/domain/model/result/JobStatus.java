package ce.chess.dockfish.domain.model.result;

public enum JobStatus {
  ACTIVE, NOT_ACTIVE
}
