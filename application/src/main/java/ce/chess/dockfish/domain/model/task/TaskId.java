package ce.chess.dockfish.domain.model.task;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Strings;
import java.util.Locale;
import java.util.UUID;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(allParameters = true)
@JsonSerialize(as = ImmutableTaskId.class)
@JsonDeserialize(as = ImmutableTaskId.class)
public interface TaskId {
  @JsonValue
  String rawId();

  static TaskId createNew() {
    return ImmutableTaskId.of(UUID.randomUUID().toString().toLowerCase(Locale.getDefault()));
  }

  default boolean matches(TaskId other) {
    return !Strings.isNullOrEmpty(other.rawId()) && rawId().startsWith(other.rawId());
  }
}
