package ce.chess.dockfish.domain.model.staticevaluation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableStaticEvaluationRequest.class)
@JsonDeserialize(as = ImmutableStaticEvaluationRequest.class)
@Value.Style(allParameters = true)
public interface StaticEvaluationRequest {
  String fen();
}
