package ce.chess.dockfish.domain.model.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.immutables.value.Value;
import org.immutables.value.Value.Style.ImplementationVisibility;

@Value.Immutable
@Value.Style(visibility = ImplementationVisibility.PRIVATE)
@JsonDeserialize
@JsonSerialize
public abstract class Score {

  public static Score fromCentiPawns(int centiPawns) {
    return new ScoreBuilder().centiPawns(centiPawns).build();
  }

  @JsonValue
  public String score() {
    return toString();
  }

  @Override
  public String toString() {
    String score = BigDecimal.valueOf(centiPawns() / 100.)
        .setScale(2, RoundingMode.HALF_UP)
        .toPlainString();
    return (centiPawns() >= 0 ? "+" : "") + score;
  }

  @Value.Parameter
  @JsonIgnore
  public abstract int centiPawns();

}
