package ce.chess.dockfish.domain.model.result;

import ce.chess.commontech.resource.jaxrs.Representation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableEvaluationMessage.class)
@JsonDeserialize(as = ImmutableEvaluationMessage.class)
@Schema(name = "EvaluationMessage")
public interface EvaluationMessage {

  Optional<String> taskName();

  String analysedPgn();

  String analysedFen();

  String uciEngineName();

  Optional<Integer> taskDepth();

  Optional<Duration> taskDuration();

  String hostname();

  JobStatus status();

  @Schema(implementation = ImmutableEvaluation.class)
  Evaluation evaluation();

  LocalDateTime taskStarted();

  Optional<LocalDateTime> lastAlive();

  @Schema(type = SchemaType.ARRAY, implementation = ImmutableEngineInformation.class)
  List<EngineInformation> latestEvents();

  List<String> history();
}
