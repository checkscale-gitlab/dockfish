package ce.chess.dockfish.infrastructure.messaging;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
class FallbackToFilePublisher {
  private static final Logger LOGGER = LogManager.getLogger(FallbackToFilePublisher.class);

  private static final String PARENT_FOLDER = "unpublished";
  private static final String FILE_PREFIX = "fallback_";
  private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("yyyyMMdd-HHmm");

  void writeToFile(@Observes PublishFailed publishFailed) {
    String timestamp = LocalDateTime.now(ZoneId.systemDefault()).format(DATE_PATTERN);
    String fileName = FILE_PREFIX + publishFailed.exchangeName() + "_" + timestamp + ".json";

    File file = Paths.get(PARENT_FOLDER, fileName).toFile();
    LOGGER.warn("Write message to {}", file.getAbsolutePath());

    boolean directoryExists = file.getParentFile().exists() || file.getParentFile().mkdirs();
    if (!directoryExists) {
      LOGGER.error("Directory does not exist {}", file.getAbsolutePath());
    }

    try (PrintWriter out = new PrintWriter(file, StandardCharsets.UTF_8)) {
      out.write(publishFailed.message());
    } catch (IOException ex) {
      LOGGER.error("Failed to write fallback file", ex);
    }
  }
}
