package ce.chess.dockfish.infrastructure.engine;

import ce.chess.dockfish.domain.event.AnalysisFinished;
import ce.chess.dockfish.domain.event.ImmutableAnalysisFinished;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableEngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.TaskId;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import raptor.engine.uci.UCIBestMove;
import raptor.engine.uci.UCIEngine;
import raptor.engine.uci.UCIOption;

@Singleton
public class EngineController {

  private static final Logger sLogger = LogManager.getLogger(EngineController.class);
  private static final String STATIC_EVALUATION_ENGINE = "stockfish";

  private final Semaphore singleProcessMutex = new Semaphore(1);

  @Inject
  UciEngineHolder uciEngineHolder;

  @Inject
  SimpleWatchdog simpleWatchdog;

  @Inject
  Event<AnalysisFinished> analysisFinishedPublisher;

  private EngineAnalysisRequest analysisJob;

  @Gauge(name = "task_time_remaining", absolute = true, unit = MetricUnits.SECONDS)
  public Long getTaskTimeRemaining() {
    return Optional.ofNullable(analysisJob)
        .filter(job -> uciEngineIsRunning(job.taskId()))
        .flatMap(EngineAnalysisRequest::estimatedCompletionTime)
        .map(completionDate -> Duration.between(LocalDateTime.now(ZoneId.systemDefault()), completionDate))
        .map(Duration::toSeconds)
        .filter(seconds -> seconds > 0)
        .orElse(0L);
  }

  public EngineAnalysisRequest startAnalysis(EngineAnalysisRequest analysisJob, EngineListener engineListener) {
    throwIfNotLocked("startAnalysis should be called only after a Lock was acquired");
    sLogger.info("START Analysis");
    UCIEngine uciEngine = uciEngineHolder.connect(
        analysisJob.engineProgramName(),
        analysisJob.initialPv(),
        analysisJob.engineOptions());

    String currentFen = analysisJob.startingPosition().fen();

    this.analysisJob = ImmutableEngineAnalysisRequest.copyOf(analysisJob)
        .withUciEngineName(uciEngine.getEngineName())
        .withStartingFen(currentFen);
    engineListener.assignTo(this.analysisJob);

    uciEngine.setPosition(currentFen);
    uciEngine.go(createGoParameter(this.analysisJob), engineListener);

    sLogger.info("*** Engine {} is running: {}", uciEngine.getEngineName(), uciEngine.isProcessingGo());
    simpleWatchdog.watch(this::uciEngineIsRunning, 1, this::watchdogFinished);

    return this.analysisJob;
  }

  void watchdogFinished() {
    sLogger.info("Watchdog: Engine finished");
    analysisFinishedPublisher.fire(ImmutableAnalysisFinished.of(analysisJob.taskId(), Instant.now()));

    stop();
    uciEngineHolder.disconnect();

    releaseLock();
  }

  public String retrieveStaticEvaluation(String fen) {
    throwIfNotLocked("retrieveStaticEvaluation should be called only after a Lock was acquired");
    sLogger.info("START eval");

    UCIEngine uciEngine = uciEngineHolder.connect(STATIC_EVALUATION_ENGINE);

    uciEngine.setPosition(fen);

    String eval = uciEngine.eval();
    sLogger.info("Got static evaluation: {}", eval);
    uciEngineHolder.disconnect();
    return eval;
  }

  public void reducePvTo(int newPv) {
    if (newPv > 0) {
      UCIEngine uciEngine = uciEngineHolder.getEngine();
      UCIOption uciOption = uciEngine.getOption("MultiPV");
      int pvCurrent = Integer.parseInt(uciOption.getValue());
      sLogger.info("Set new MultiPV from {} to {}", pvCurrent, newPv);
      if (newPv < pvCurrent) {
        // + set new PV to engine
        uciOption.setValue(Integer.toString(newPv));
        uciEngine.setOption(uciOption);
      }
    }
  }

  public void stop() {
    if (uciEngineIsRunning()) {
      sLogger.info("stop");
      UCIBestMove bestMove = uciEngineHolder.getEngine().stop();
      sLogger.info("Engine stopped. BestMove: {}", bestMove);
    }
  }

  public boolean uciEngineIsRunning(TaskId taskId) {
    return uciEngineIsRunning()
        && Objects.nonNull(analysisJob)
        && Objects.nonNull(analysisJob.taskId())
        && analysisJob.taskId().matches(taskId);
  }

  public boolean uciEngineIsRunning() {
    return uciEngineHolder.getEngine().isProcessingGo();
  }

  private String createGoParameter(EngineAnalysisRequest analysisJob) {
    StringBuilder goCommandBuilder = new StringBuilder(20);

    analysisJob.maxDepth().ifPresent(p -> goCommandBuilder.append(" depth ").append(p));
    analysisJob.maxDuration().ifPresent(p -> goCommandBuilder.append(" movetime ").append(p.toMillis()));

    String goCommand = goCommandBuilder.toString().trim();
    if (goCommand.isEmpty()) {
      goCommand = "infinite";
    }
    return goCommand;
  }

  public void acquireLock() {
    sLogger.info("START wait for lock");
    singleProcessMutex.acquireUninterruptibly();
    sLogger.info("END   wait for lock");
  }

  public boolean tryAcquireLock() {
    sLogger.info("Start try acquire lock");
    boolean acquire = singleProcessMutex.tryAcquire();
    sLogger.info("End try acquire lock = {}", acquire);
    return acquire;
  }

  public void releaseLock() {
    sLogger.info("Release Lock");
    if (singleProcessMutex.availablePermits() == 0) {
      singleProcessMutex.release();
      sLogger.info("Lock released");
    } else {
      sLogger.warn("Lock was already released!");
    }
  }

  public void blockWhileActive() {
    sLogger.info("START Block while running");
    simpleWatchdog.waitWhileConditionIsTrue();
    sLogger.info("END   Block while running");
  }

  private void throwIfNotLocked(String errorMessage) {
    if (singleProcessMutex.availablePermits() > 0) {
      throw new IllegalStateException(errorMessage);
    }
  }

}
