package ce.chess.dockfish.infrastructure.messaging;

import ce.chess.commontech.messaging.AbstractMessagePublisher;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class StaticEvaluationPublisher extends AbstractMessagePublisher {
  private static final Logger LOGGER = LogManager.getLogger(StaticEvaluationPublisher.class);

  private static final String EXCHANGE_EVALUATION_STATIC = "staticEvaluation.created";
  private static final String QUEUE_NAME = "dockfish.staticEvaluations";

  @Inject
  ObjectMapper objectMapper;

  public void publishFinalEvaluation(@Observes StaticEvaluation staticEvaluation) {
    try {
      String bodyContent = Strings.nullToEmpty(objectMapper.writeValueAsString(staticEvaluation));
      LOGGER.info(" [x] Publishing '{}'", bodyContent);
      publishAndClose(bodyContent);
    } catch (IOException ex) {
      LOGGER.error("Failed to publish Message.", ex);
    }
  }

  @Override
  public String getExchangeName() {
    return EXCHANGE_EVALUATION_STATIC;
  }

  @Override
  public String getQueueName() {
    return QUEUE_NAME;
  }

}
