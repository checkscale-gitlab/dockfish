package ce.chess.dockfish.infrastructure.engine;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.BooleanSupplier;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
class SimpleWatchdog {

  private static final int INITIAL_DELAY = 0;
  private CountDownLatch countDownLatch;

  public SimpleWatchdog() {
    this.countDownLatch = new CountDownLatch(0);
  }

  boolean waitWhileConditionIsTrue() {
    try {
      countDownLatch.await();
    } catch (InterruptedException ex) {
      Thread.currentThread().interrupt();
      return false;
    }
    return true;
  }

  void watch(BooleanSupplier condition, int monitoringIntervalInSeconds, Runnable performWhenFalse) {
    countDownLatch = new CountDownLatch(1);
    ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    scheduledExecutorService.scheduleAtFixedRate(() -> {
      if (!condition.getAsBoolean()) {
        scheduledExecutorService.shutdown();
        performWhenFalse.run();
        countDownLatch.countDown();
      }
    }, INITIAL_DELAY, monitoringIntervalInSeconds, SECONDS);
  }

}

