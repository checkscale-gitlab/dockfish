package ce.chess.dockfish.infrastructure.messaging;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
class FallbackHttpPublisher {
  private static final Logger LOGGER = LogManager.getLogger(FallbackHttpPublisher.class);
  private static final String FALLBACK_POST_URL = "fallback_post_url";

  @Inject
  Config config;

  void postToHttpServer(@Observes PublishFailed publishFailed) {
    getRestUrl().ifPresent(url -> doPost(url, publishFailed));
  }

  private void doPost(String url, PublishFailed publishFailed) {
    LOGGER.warn("Post message to {}", url);
    try {
      URL apiUrl = new URL(url);
      HttpURLConnection httpConnection = (HttpURLConnection) apiUrl.openConnection();
      httpConnection.setDoOutput(true);
      httpConnection.setRequestMethod("POST");
      httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
      try (DataOutputStream out = new DataOutputStream(httpConnection.getOutputStream())) {
        out.write(publishFailed.message().getBytes(StandardCharsets.UTF_8));
      }
      LOGGER.info("Response code: {}", httpConnection.getResponseCode());
    } catch (IOException ex) {
      LOGGER.error("Failed to post to fallback server", ex);
    }
  }

  private Optional<String> getRestUrl() {
    return config.getOptionalValue(FALLBACK_POST_URL, String.class);
  }

}
