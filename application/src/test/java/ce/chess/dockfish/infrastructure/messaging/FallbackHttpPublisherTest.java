package ce.chess.dockfish.infrastructure.messaging;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.exactly;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import java.util.Optional;
import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import org.eclipse.microprofile.config.Config;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
class FallbackHttpPublisherTest {

  @WeldSetup
  public WeldInitiator weld = WeldInitiator
      .from(FallbackHttpPublisher.class, MockProducer.class)
      .build();

  static Config config = mock(Config.class);

  @Alternative
  @Priority(3)
  static class MockProducer {
    @Produces
    Config config() {
      return config;
    }
  }

  @BeforeEach
  void setUp() {
    Mockito.reset(config);
  }



  private static final String FALLBACK_POST_URL = "fallback_post_url";
  private static final int PORT_FOR_TEST = 8015;

  private final WireMockServer wireMockServer = new WireMockServer(options()
      .port(PORT_FOR_TEST));

  @BeforeEach
  void setup() {
    wireMockServer.start();
    configureFor("localhost", PORT_FOR_TEST);
    stubFor(post(urlPathEqualTo("/evaluations"))
        .withRequestBody(equalTo("message"))
        .willReturn(aResponse()
            .withStatus(202)));
  }

  @AfterEach
  void tearDown() {
    wireMockServer.stop();
  }


  @Test
  void givenUrlThenPost() {
    given(config.getOptionalValue(FALLBACK_POST_URL, String.class))
        .willReturn(Optional.of("http://localhost:" + PORT_FOR_TEST + "/evaluations"));

    fire(ImmutablePublishFailed.of("exchange", "message"));

    verify(postRequestedFor(urlEqualTo("/evaluations"))
        .withHeader("Content-Type", containing("application/json")));
  }

  @Test
  void givenNoUrlThenDontPost() {
    given(config.getOptionalValue(FALLBACK_POST_URL, String.class))
        .willReturn(Optional.empty());

    fire(ImmutablePublishFailed.of("exchange", "message"));

    verify(exactly(0), postRequestedFor(urlEqualTo("/evaluations")));
  }

  private void fire(PublishFailed event) {
    weld.event().select(PublishFailed.class).fire(event);
  }
}
