package ce.chess.dockfish.infrastructure.persistence;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import ce.chess.dockfish.domain.model.result.ResultGame;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableEngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskId;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InMemoryTaskRepositoryTest {
  private static final TaskId taskId1 = ImmutableTaskId.of("TASK1");
  private static final TaskId taskId2 = ImmutableTaskId.of("TASK2");
  private static final TaskId taskIdx = ImmutableTaskId.of("TASKX");
  private static final TaskId someOtherTaskId = ImmutableTaskId.of("some other TaskId");
  private static final LocalDateTime NOW = LocalDateTime.now(ZoneId.systemDefault());

  @Mock
  ResultGame resultGame;

  private EngineAnalysisRequest task1;

  private EngineAnalysisRequest task2;

  private EngineAnalysisRequest task3;

  private InMemoryTaskRepository cut;


  @BeforeEach
  void setUp() {
    cut = new InMemoryTaskRepository();

    task1 = ImmutableEngineAnalysisRequest.builder()
        .taskId(taskId1)
        .created(NOW.minusHours(1L))
        .engineProgramName("prog1")
        .hostname("testhost")
        .startingPosition(resultGame)
        .startingMoveNumber(23)
        .initialPv(23)
        .build();
    task2 = ImmutableEngineAnalysisRequest.copyOf(task1)
        .withTaskId(taskId2)
        .withCreated(NOW);
    task3 = ImmutableEngineAnalysisRequest.copyOf(task1)
        .withTaskId(someOtherTaskId)
        .withCreated(NOW.plusHours(1L));
    Stream.of(task1, task2, task3).forEach(cut::save);
    assertThat(cut.getCacheSize(), is(3L));
  }

  @Nested
  class FindByTaskId {
    @Test
    void returnsElements() {

      Optional<EngineAnalysisRequest> result = cut.findByTaskId(taskId1);

      assertThat(result, is(Optional.of(task1)));
    }

    @Test
    void searchesByPartialTaskId() {

      Optional<EngineAnalysisRequest> result = cut.findByTaskId(ImmutableTaskId.of("some"));

      assertThat(result, is(Optional.of(task3)));
    }
  }

  @Nested
  class FindLatest {
    @Test
    void returnsElementWithLatestCreationDate() {

      Optional<EngineAnalysisRequest> result = cut.findLatest();

      assertThat(result, is(Optional.of(task3)));
    }
  }

  @Nested
  class HasDuplicates {
    @Test
    void isTrueForSimilarTasks() {
      EngineAnalysisRequest other = ImmutableEngineAnalysisRequest.copyOf(task3).withTaskId(taskIdx);

      assertThat(cut.hasDuplicate(other), is(true));
    }

    @Test
    void isFalseForDifferentTasks() {
      EngineAnalysisRequest other = ImmutableEngineAnalysisRequest.copyOf(task2).withName("differentName");
      assertThat(cut.hasDuplicate(other), is(false));
    }

  }

}
