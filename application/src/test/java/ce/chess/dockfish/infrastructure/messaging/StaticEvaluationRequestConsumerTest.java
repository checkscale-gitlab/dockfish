package ce.chess.dockfish.infrastructure.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static wiremock.org.hamcrest.Matchers.empty;

import ce.chess.commontech.messaging.Message;
import ce.chess.dockfish.application.StaticEvaluationService;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluationRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UncheckedIOException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StaticEvaluationRequestConsumerTest {
  @Mock
  private StaticEvaluationService service;
  @Mock
  private ObjectMapper objectMapper;
  @InjectMocks
  private StaticEvaluationRequestConsumer cut;

  @Test
  void doesCallService() throws IOException {
    StaticEvaluationRequest request = mock(StaticEvaluationRequest.class);
    given(objectMapper.readValue(new byte[]{42}, StaticEvaluationRequest.class)).willReturn(request);

    cut.handleMessage(new Message().withBody(new byte[]{42}));

    verify(service).createAndPublishEvaluation(request);
  }

  @Test
  void failsIfDeserialisationFails() throws IOException {
    given(objectMapper.readValue(new byte[]{42}, StaticEvaluationRequest.class)).willThrow(IOException.class);

    assertThrows(UncheckedIOException.class, () -> cut.handleMessage(new Message().withBody(new byte[]{42})));
  }

  @Test
  void hasBindingParameters() {
    assertThat(cut.getExchangeName(), is(not(blankOrNullString())));
    assertThat(cut.getQueueNames(), is(not(empty())));
    assertThat(cut.getExchangeType(), is("TOPIC"));
    assertThat(cut.isAutoAck(), is(false));
  }

  @Test
  void returnsPrefetchCountOne() {
    assertThat(cut.getPrefetchLimit(), is(1));
  }

}
