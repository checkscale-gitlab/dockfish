package ce.chess.dockfish.infrastructure.engine;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ce.chess.dockfish.domain.model.result.ResultGame;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskId;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import javax.enterprise.event.Event;
import org.awaitility.Awaitility;
import org.awaitility.core.ConditionTimeoutException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import raptor.engine.uci.UCIEngine;
import raptor.engine.uci.UCIOption;
import raptor.engine.uci.options.UCISpinner;

@ExtendWith(MockitoExtension.class)
class EngineControllerTest {

  public static final String EXPECTED_FEN = "rnbqkbnr/ppp1pppp/8/3p4/3P4/8/PPP1PPPP/RNBQKBNR w KQkq d6 0 2";
  public static final String EXPECTED_ENGINE_NAME = "uciEngine.getEngineName";
  private static final LocalDateTime NOW = LocalDateTime.now(ZoneId.systemDefault());
  @Mock
  private UCIEngine uciEngine;

  @Mock
  private UciEngineHolder uciEngineHolder;

  @Mock
  private EngineListener engineListener;

  @Mock
  private SimpleWatchdog simpleWatchdog;

  @Mock
  private Event<AnalysisFinished> analyisFinishedPublisher;

  @Mock
  private EngineAnalysisRequest engineTask;

  @Captor
  private ArgumentCaptor<EngineAnalysisRequest> taskAssignedToEngineListener;

  @InjectMocks
  private EngineController cut;

  @Nested
  class StartAnalysis {
    @Nested
    class GivenLockIsAcquired {
      @BeforeEach
      void beforeEach() {
        given(uciEngineHolder.connect(anyString(), anyInt(), any())).willReturn(uciEngine);
        given(uciEngine.getEngineName()).willReturn("anything");
        given(engineTask.taskId()).willReturn(ImmutableTaskId.of("taskId"));
        given(engineTask.engineProgramName()).willReturn("engineName");
        given(engineTask.hostname()).willReturn("testhost");
        given(engineTask.startingPosition()).willReturn(ResultGame.fromPgn("1. d4 d5"));
        given(engineTask.created()).willReturn(NOW);
        given(engineTask.initialPv()).willReturn(4);

        cut.acquireLock();
      }

      @Test
      void doesStartEngineInOrder() {

        cut.startAnalysis(engineTask, engineListener);

        InOrder inOrder = Mockito.inOrder(uciEngineHolder, engineListener, uciEngine, simpleWatchdog);
        inOrder.verify(uciEngineHolder).connect("engineName", 4, List.of());
        inOrder.verify(engineListener).assignTo(taskAssignedToEngineListener.capture());
        assertThat(taskAssignedToEngineListener.getValue(), is(not(equalTo(engineTask))));
        inOrder.verify(uciEngine).setPosition(eq(EXPECTED_FEN), any());
        inOrder.verify(uciEngine).go("infinite", engineListener);
        inOrder.verify(simpleWatchdog).watch(any(), eq(1), any());
      }

      @Test
      void doesRunMaxDepth() {
        given(engineTask.maxDepth()).willReturn(Optional.of(5));

        cut.startAnalysis(engineTask, engineListener);

        verify(uciEngine).go("depth 5", engineListener);
      }

      @Test
      void doesRunMaxDuration() {
        given(engineTask.maxDuration()).willReturn(Optional.of(Duration.ofMillis(200L)));

        cut.startAnalysis(engineTask, engineListener);

        verify(uciEngine).go("movetime 200", engineListener);
      }

      @Test
      void doesReturnTaskWithUciEngineNameAndFen() {
        given(engineTask.maxDuration()).willReturn(Optional.of(Duration.ofMillis(200L)));
        given(uciEngine.getEngineName()).willReturn(EXPECTED_ENGINE_NAME);

        EngineAnalysisRequest result = cut.startAnalysis(engineTask, engineListener);

        assertThat(result.uciEngineName().orElseThrow(AssertionError::new), is(equalTo(EXPECTED_ENGINE_NAME)));
        assertThat(result.startingFen().orElseThrow(AssertionError::new), is(equalTo(EXPECTED_FEN)));

        verify(engineListener).assignTo(taskAssignedToEngineListener.capture());
        assertThat(taskAssignedToEngineListener.getValue().uciEngineName(), is(Optional.of(EXPECTED_ENGINE_NAME)));
        assertThat(taskAssignedToEngineListener.getValue().startingFen(), is(Optional.of(EXPECTED_FEN)));
      }

      @Test
      void doesKeepLockAfterSuccess() {

        cut.startAnalysis(engineTask, engineListener);

        assertThat(cut.tryAcquireLock(), is(false));
      }

    }

    @Test
    void startAnalysisThrowsIfNotLocked() {
      IllegalStateException illegalStateException =
          assertThrows(IllegalStateException.class, () -> cut.startAnalysis(engineTask, engineListener));
      assertThat(illegalStateException.getMessage(),
          containsString("startAnalysis should be called only after a Lock was acquired"));
      assertThat(cut.tryAcquireLock(), is(true));
    }
  }

  @Nested
  class Stop {
    @Test
    void doesCallStopOnEngine() {
      given(uciEngineHolder.getEngine()).willReturn(uciEngine);
      given(uciEngine.isProcessingGo()).willReturn(true);

      cut.stop();

      verify(uciEngine).isProcessingGo();
      verify(uciEngine).stop();
    }
  }

  @Nested
  class StaticEvaluation {
    @Test
    void returnsEvaluationFromEngine() {
      given(uciEngineHolder.connect(anyString())).willReturn(uciEngine);
      given(uciEngine.eval()).willReturn("anyEvaluation");

      cut.acquireLock();
      String staticEvaluation = cut.retrieveStaticEvaluation("anyFen");

      assertThat(staticEvaluation, is(equalTo("anyEvaluation")));
      verify(uciEngine).eval();
      verify(uciEngineHolder).disconnect();
    }

    @Test
    void staticEvaluationThrowsIfNotLocked() {
      IllegalStateException illegalStateException =
          assertThrows(IllegalStateException.class, () -> cut.retrieveStaticEvaluation("anyFen"));
      assertThat(illegalStateException.getMessage(),
          containsString("retrieveStaticEvaluation should be called only after a Lock was acquired"));
    }

  }

  @Nested
  class TestIsRunning {
    @Test
    void returnTrueIfActiveAndTaskIdMatches() {
      TaskId taskId = ImmutableTaskId.of("42");
      given(engineTask.taskId()).willReturn(taskId);
      given(uciEngineHolder.getEngine()).willReturn(uciEngine);
      given(uciEngine.isProcessingGo()).willReturn(true);

      boolean active = cut.uciEngineIsRunning(taskId);

      assertThat(active, is(true));
    }
  }

  @Nested
  class AnalysisFinished {
    @Test
    void firesEventAndReleases() {
      TaskId taskId = ImmutableTaskId.of("42");
      given(engineTask.taskId()).willReturn(taskId);
      given(uciEngineHolder.getEngine()).willReturn(uciEngine);
      given(uciEngine.isProcessingGo()).willReturn(true);
      cut.acquireLock();

      cut.watchdogFinished();

      verify(analyisFinishedPublisher).fire(any());
      verify(uciEngine).stop();
      verify(uciEngineHolder).disconnect();
      assertThat(cut.tryAcquireLock(), is(true));
    }
  }

  @Nested
  class TestReducePv {
    private UCIOption multiPvOptionFixture(int value) {
      UCIOption oldOption = new UCISpinner();
      oldOption.setName("MultiPV");
      oldOption.setValue("" + value);
      return oldOption;
    }

    @Test
    void doesNothingIfNewPvIsZero() {

      cut.reducePvTo(0);

      verify(uciEngine, never()).getOption(any());
      verify(uciEngine, never()).setOption(any());
    }

    @Test
    void doesNotIncreasePv() {
      given(uciEngineHolder.getEngine()).willReturn(uciEngine);
      UCIOption oldOption = multiPvOptionFixture(1);
      given(uciEngine.getOption("MultiPV")).willReturn(oldOption);

      cut.reducePvTo(2);

      verify(uciEngine, never()).setOption(any());
    }

    @Test
    void doesReducePv() {
      given(uciEngineHolder.getEngine()).willReturn(uciEngine);
      UCIOption oldOption = multiPvOptionFixture(10);
      given(uciEngine.getOption("MultiPV")).willReturn(oldOption);

      cut.reducePvTo(2);

      ArgumentCaptor<UCIOption> optionCaptor = ArgumentCaptor.forClass(UCIOption.class);
      verify(uciEngine).setOption(optionCaptor.capture());
      assertThat(optionCaptor.getValue().getName(), is("MultiPV"));
      assertThat(optionCaptor.getValue().getValue(), is("2"));
    }
  }

  @Nested
  class BlocksParallelExecutions {
    @AfterEach
    void releaseLock() {
      cut.releaseLock();
    }

    @Test
    void whenLockIsAcquiredThenNoLockCanBeGained() {
      cut.acquireLock();
      assertThat(cut.tryAcquireLock(), is(false));

      cut.releaseLock();
      assertThat(cut.tryAcquireLock(), is(true));
    }

    @Test
    void whenLockIsAcquiredWithTryThenNoLockCanBeGained() {
      assertThat(cut.tryAcquireLock(), is(true));
      assertThat(cut.tryAcquireLock(), is(false));
    }

    @Test
    @SuppressWarnings("squid:S5778")
    void secondAcquireBlocks() {
      cut.acquireLock();
      Awaitility.pollExecutorService(Executors.newSingleThreadExecutor());
      assertThrows(ConditionTimeoutException.class,
          () -> await().atMost(500, MILLISECONDS).until(() -> {
            cut.acquireLock();
            return true;
          }));
    }

    @Test
    void secondReleaseDoesNothing() {
      cut.acquireLock();
      cut.releaseLock();
      // will finish immediately
      cut.releaseLock();
      assertThat(cut.tryAcquireLock(), is(true));
    }

    @Test
    void blocksWhileRunning() {
      cut.blockWhileActive();
      verify(simpleWatchdog).waitWhileConditionIsTrue();
    }
  }

  @Nested
  class DoesExposeMetrics {
    @BeforeEach
    void givenAnalysisIsRunning() {
      given(uciEngineHolder.getEngine()).willReturn(uciEngine);
      given(uciEngine.isProcessingGo()).willReturn(true);
      given(engineTask.taskId()).willReturn(ImmutableTaskId.of("taskId"));
    }

    @Test
    void whenCompletionInFutureThenReturnSecondsRemaining() {
      given(engineTask.estimatedCompletionTime()).willReturn(Optional.of(NOW.plusHours(1).plusMinutes(1)));

      Long expected = cut.getTaskTimeRemaining();

      assertThat(expected, is(greaterThan(3600L)));
    }

    @Test
    void whenCompletionInPastThenReturnZero() {
      given(engineTask.estimatedCompletionTime()).willReturn(Optional.of(NOW.minusHours(1)));

      Long expected = cut.getTaskTimeRemaining();

      assertThat(expected, is(0L));
    }

    @Test
    void whenFutureButEngineNotRunningThenReturnZero() {
      given(uciEngine.isProcessingGo()).willReturn(false);
      lenient().when(engineTask.estimatedCompletionTime()).thenReturn(Optional.of(NOW.plusHours(1).plusMinutes(1)));

      Long expected = cut.getTaskTimeRemaining();

      assertThat(expected, is(0L));
    }
  }
}
