package ce.chess.dockfish.infrastructure.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
class FallbackToFilePublisherTest {
  @WeldSetup
  public WeldInitiator weld = WeldInitiator
      .from(FallbackToFilePublisher.class, FallbackToFilePublisherTest.class)
      .build();

  private File expectedFile;

  @BeforeEach
  void setUp() {
    LocalDateTime now = LocalDateTime.now(ZoneId.systemDefault());
    expectedFile = new File(
        "unpublished/fallback_" + "exchange_" + now.format(DateTimeFormatter.ofPattern("yyyyMMdd-HHmm")) + ".json");
  }

  @AfterEach
  void removeFile() {
    if (expectedFile != null && expectedFile.exists()) {
      expectedFile.delete();
    }
  }

  @Test
  void doesObserveEventAndWritesFile() throws IOException {
    fire(ImmutablePublishFailed.of("exchange", "message"));

    assertThat(expectedFile.exists(), is(true));
    assertThat(Files.asCharSource(expectedFile, StandardCharsets.UTF_8).read(), is(equalTo("message")));
  }

  private void fire(PublishFailed event) {
    weld.event().select(PublishFailed.class).fire(event);
  }

}
