package ce.chess.dockfish.infrastructure.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import ce.chess.commontech.messaging.Message;
import ce.chess.commontech.messaging.rabbit.RabbitSender;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
class StaticEvaluationPublisherTest {
  @WeldSetup
  private WeldInitiator weld = WeldInitiator
      .from(StaticEvaluationPublisher.class, MockProducer.class)
      .build();

  private static final String MESSAGE_BODY = "messageContent";

  static ObjectMapper objectMapper = mock(ObjectMapper.class);

  static RabbitSender rabbitSender = mock(RabbitSender.class);

  @Priority(2)
  static class MockProducer {
    @Produces
    @Alternative
    ObjectMapper objectMapper() {
      return objectMapper;
    }

    @Produces
    @Alternative
    RabbitSender rabbitSender() {
      return rabbitSender;
    }
  }

  @BeforeEach
  void setUp() {
    Mockito.reset(objectMapper, rabbitSender);
  }


  private Message expectedMessage = new Message()
      .withExchange("staticEvaluation.created")
      .withRoutingKey("")
      .withContentType("application/json")
      .asPersistent()
      .withBody(MESSAGE_BODY);

  @Mock
  private StaticEvaluation publishEvent;

  @Inject
  StaticEvaluationPublisher cut;

  private void fire(StaticEvaluation event) {
    weld.event().select(StaticEvaluation.class).fire(event);
  }
  //

  @Test
  void hasBindingParameters() {
    assertThat(cut.getExchangeName(), is(not(blankOrNullString())));
    assertThat(cut.getQueueName(), is(not(blankOrNullString())));
  }

  @Test
  void publishesMessage() throws IOException {
    given(objectMapper.writeValueAsString(publishEvent)).willReturn(MESSAGE_BODY);

    fire(publishEvent);

    verify(rabbitSender).publish(refEq(expectedMessage));
    verify(rabbitSender).close();
  }

  @Test
  void failsSafeley() throws IOException {
    given(objectMapper.writeValueAsString(publishEvent)).willThrow(JsonProcessingException.class);

    fire(publishEvent);

    verifyNoInteractions(rabbitSender);
  }
}
