package ce.chess.dockfish.domain.model.task;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class TaskIdTest {

  TaskId cut;

  @Test
  void testMatches() {
    cut = ImmutableTaskId.of("abcdefghij");

    assertThat(cut.matches(ImmutableTaskId.of("abc")), is(true));
    assertThat(cut.matches(ImmutableTaskId.of("abv")), is(false));
    assertThat(cut.matches(ImmutableTaskId.copyOf(cut)), is(true));
    assertThat(cut.matches(ImmutableTaskId.of("")), is(false));
    assertThat(ImmutableTaskId.of("abc").matches(cut), is(false));
  }

  @Test
  void testCreateNew() {
    cut = TaskId.createNew();

    assertThat(cut.rawId(), is(not(blankOrNullString())));
    assertThat(UUID.fromString(cut.rawId()), isA(UUID.class));
  }

}
