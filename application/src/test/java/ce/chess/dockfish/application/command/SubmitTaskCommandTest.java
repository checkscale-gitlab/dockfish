package ce.chess.dockfish.application.command;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;
import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class SubmitTaskCommandTest {

  private SubmitTaskCommand cut;

  @Test
  @SuppressWarnings({"squid:S5778", "ResultOfMethodCallIgnored"})
  void mustHaveDurationOrDepth() {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
        () -> ImmutableSubmitTaskCommand.builder()
            .initialPv(3)
            .engineId("engineId")
            .name("name")
            .pgn("pgn")
            .build());
    assertThat(thrown.getMessage(), containsString("Either Depth or Duration must be given"));
  }

  @Test
  void parsesDuration() {
    cut = ImmutableSubmitTaskCommand.builder()
        .maxDuration(Optional.of(Duration.parse("PT2H30M")))
        .initialPv(3)
        .engineId("engineId")
        .name("name")
        .pgn("pgn")
        .build();
    assertThat(cut.maxDuration(), is(Optional.of(Duration.ofMinutes(2 * 60 + 30))));
  }

  @Test
  void nameMayBeEmpty() {
    cut = ImmutableSubmitTaskCommand.builder()
        .maxDuration(Optional.of(Duration.parse("PT2H30M")))
        .initialPv(3)
        .engineId("engineId")
        .pgn("pgn")
        .build();
    assertThat(cut.name().isPresent(), is(false));
  }

  @Nested
  class SetsOptions {
    EngineOption newOption = ImmutableEngineOption.of("newOption", "newValue");
    EngineOption oldOption = ImmutableEngineOption.of("newOption", "oldValue");
    EngineOption otherOption = ImmutableEngineOption.of("otherOption", "newValue");

    @BeforeEach
    void setUp() {
      cut = ImmutableSubmitTaskCommand.builder()
          .maxDuration(Optional.of(Duration.parse("PT2H30M")))
          .initialPv(3)
          .engineId("engineId")
          .name("name")
          .pgn("pgn")
          .build();
    }

    @Nested
    class WhenNoOptionsArePresent {

      @Test
      void newOptionsWillBeCreated() {
        SubmitTaskCommand result = cut.addOrReplaceOption(newOption);
        assertThat(result.options(), contains(newOption));
      }
    }

    @Nested
    class WhenOptionIsAlreadyPresent {
      @Test
      void theNewOptionWillReplaceTheExistingOne() {
        cut = ImmutableSubmitTaskCommand.copyOf(cut).withOptions(Collections.singletonList(oldOption));

        SubmitTaskCommand result = cut.addOrReplaceOption(newOption);

        assertThat(result.options(), contains(newOption));
      }

    }

    @Nested
    class WhenDifferentOptionsArePresent {
      @Test
      void theNewOptionWillBeAdded() {
        cut = ImmutableSubmitTaskCommand.copyOf(cut).withOptions(Collections.singletonList(otherOption));

        SubmitTaskCommand result = cut.addOrReplaceOption(newOption);

        assertThat(result.options(), containsInAnyOrder(otherOption, newOption));
      }

    }
  }
}
