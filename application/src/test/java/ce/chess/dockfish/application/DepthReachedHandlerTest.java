package ce.chess.dockfish.application;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import ce.chess.dockfish.domain.event.DepthReached;
import ce.chess.dockfish.domain.event.ImmutableDepthReached;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.EvaluationRepository;
import ce.chess.dockfish.domain.model.result.Variation;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskRepository;

import java.util.List;
import java.util.Optional;
import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import nl.altindag.log.LogCaptor;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
class DepthReachedHandlerTest {
  @WeldSetup
  private WeldInitiator weld = WeldInitiator
      .from(DepthReachedHandler.class, MockProducer.class)
      .build();

  private static final DepthReached event = ImmutableDepthReached.of(ImmutableTaskId.of("task"), 40);

  static EvaluationRepository evaluationRepository = mock(EvaluationRepository.class);
  static TaskRepository engineTaskRepository = mock(TaskRepository.class);
  static AdaptPvService adaptPvService = mock(AdaptPvService.class);

  @Priority(1)
  static class MockProducer {
    @Produces
    @Alternative
    EvaluationRepository evaluationRepository() {
      return evaluationRepository;
    }

    @Produces
    @Alternative
    TaskRepository engineTaskRepository() {
      return engineTaskRepository;
    }

    @Produces
    @Alternative
    AdaptPvService adaptPvService() {
      return adaptPvService;
    }
  }

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private Evaluation evaluation;

  @Mock
  private EngineAnalysisRequest engineTask;

  private static LogCaptor logCaptor;

  @BeforeAll
  public static void setupLogCaptor() {
    logCaptor = LogCaptor.forClass(DepthReachedHandler.class);
  }

  @AfterEach
  public void clearLogCaptor() {
    logCaptor.clearLogs();
  }

  @AfterAll
  public static void closeLogCaptor() {
    logCaptor.resetLogLevel();
    logCaptor.close();
  }

  @BeforeEach
  void setUp() {
    Mockito.reset(evaluationRepository, engineTaskRepository, adaptPvService);
    given(engineTaskRepository.findByTaskId(event.taskId())).willReturn(Optional.of(engineTask));
    given(evaluationRepository.findByTaskIdMaxCreated(event.taskId())).willReturn(Optional.of(evaluation));
    lenient().when(evaluation.variations())
        .thenReturn(List.of(mock(Variation.class), mock(Variation.class), mock(Variation.class)));
  }

  @Test
  void observesEvent() {

    fire(event);

    assertThat(logCaptor.getInfoLogs(), hasItem(containsString("d0")));
    verify(engineTaskRepository).findByTaskId(event.taskId());
    verify(adaptPvService).adaptPv(evaluation, engineTask);
  }


  @Test
  void givenNoTaskThenJustLog() {
    given(engineTaskRepository.findByTaskId(event.taskId())).willReturn(Optional.empty());

    fire(event);

    assertThat(logCaptor.getWarnLogs(), hasItem(containsString("Task not found")));
    verifyNoInteractions(adaptPvService);
    verifyNoInteractions(evaluation);
  }


  private void fire(DepthReached event) {
    weld.event().select(DepthReached.class).fire(event);
  }

}
