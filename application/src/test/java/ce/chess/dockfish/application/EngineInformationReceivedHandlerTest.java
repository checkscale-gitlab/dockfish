package ce.chess.dockfish.application;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ce.chess.dockfish.domain.event.DepthReached;
import ce.chess.dockfish.domain.event.EngineInformationReceived;
import ce.chess.dockfish.domain.event.ImmutableDepthReached;
import ce.chess.dockfish.domain.event.ImmutableEngineInformationReceived;
import ce.chess.dockfish.domain.model.engine.EngineInformationReceivedRepository;
import ce.chess.dockfish.domain.model.engine.ShortLineReplacer;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.EvaluationRepository;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
class EngineInformationReceivedHandlerTest {
  @WeldSetup
  private WeldInitiator weld = WeldInitiator
      .from(EngineInformationReceivedHandler.class, MockProducer.class)
      .build();

  private static final EngineInformationReceived infoEvent = ImmutableEngineInformationReceived.builder()
      .calculatedPlies(5)
      .depth(20)
      .lineSan("2... Nf6 3. Nc3 e6 4. Qc2 g6")
      .multiPv(1)
      .nodes(2000)
      .nodesPerSecond(1000)
      .occurredOn(LocalDateTime.now(ZoneId.systemDefault()))
      .pgn("1.d4 d5 2.c4 Nf6 3. Nc3 e6 4. Qc2 g6*")
      .score(30)
      .taskId(ImmutableTaskId.of("task1"))
      .tbHits(0L)
      .time(1000 * 60 * 100L)
      .build();

  static EngineInformationReceivedRepository eventRepository = mock(EngineInformationReceivedRepository.class);

  static EvaluationRepository evaluationRepository = mock(EvaluationRepository.class);

  static Event<DepthReached> newDepthEvent = mock(Event.class);

  static ShortLineReplacer shortLineReplacer = mock(ShortLineReplacer.class);

  @Priority(11)
  static class MockProducer {
    @Produces
    @Alternative
    EngineInformationReceivedRepository eventRepository() {
      return eventRepository;
    }

    @Produces
    @Alternative
    EvaluationRepository evaluationRepository() {
      return evaluationRepository;
    }

    @Produces
    @Alternative
    Event<DepthReached> newDepthEvent() {
      return newDepthEvent;
    }

    @Produces
    @Alternative
    ShortLineReplacer shortLineReplacer() {
      return shortLineReplacer;
    }
  }

  @BeforeEach
  void setUp() {
    Mockito.reset(eventRepository, evaluationRepository, newDepthEvent, shortLineReplacer);
  }

  @Test
  void observesUciInformationReceivedEvent() {
    given(shortLineReplacer.fillUpGameIfTooShort(infoEvent)).willReturn(infoEvent);
    given(eventRepository.findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(infoEvent.taskId()))
        .willReturn(Collections.singletonList(infoEvent));

    fire(infoEvent);

    InOrder inOrder = Mockito.inOrder(shortLineReplacer, eventRepository, evaluationRepository);
    inOrder.verify(shortLineReplacer).fillUpGameIfTooShort(infoEvent);
    inOrder.verify(eventRepository).save(infoEvent);
    inOrder.verify(evaluationRepository).save(any(Evaluation.class));
  }

  @Test
  void doesNotCreateEvaluationWhenNoGame() {
    EngineInformationReceived eventSansGame = ImmutableEngineInformationReceived.copyOf(infoEvent).withPgn("");
    given(shortLineReplacer.fillUpGameIfTooShort(eventSansGame)).willReturn(eventSansGame);

    fire(eventSansGame);

    verify(eventRepository).save(eventSansGame);
    verify(evaluationRepository, never()).save(any(Evaluation.class));
  }

  @Test
  void whenAllVariationsHaveSameDepth_firesNewDepthEvent() {
    given(shortLineReplacer.fillUpGameIfTooShort(infoEvent)).willReturn(infoEvent);
    given(eventRepository.findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(infoEvent.taskId()))
        .willReturn(Arrays.asList(infoEvent, infoEvent));

    EngineInformationReceivedHandler cut =
        new EngineInformationReceivedHandler(eventRepository, evaluationRepository, newDepthEvent, shortLineReplacer);
    cut.receive(infoEvent);

    verify(eventRepository).findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(infoEvent.taskId());
    verify(newDepthEvent).fire(ImmutableDepthReached.of(infoEvent.taskId(), infoEvent.depth()));
  }

  @Test
  void whenNotAllVariationsHaveSameDepth_firesNoNewDepthEvent() {
    given(shortLineReplacer.fillUpGameIfTooShort(infoEvent)).willReturn(infoEvent);
    EngineInformationReceived infoEvent2 = ImmutableEngineInformationReceived.copyOf(infoEvent).withDepth(22);
    given(eventRepository.findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(infoEvent.taskId()))
        .willReturn(Arrays.asList(infoEvent, infoEvent2));

    EngineInformationReceivedHandler cut =
        new EngineInformationReceivedHandler(eventRepository, evaluationRepository, newDepthEvent, shortLineReplacer);
    cut.receive(infoEvent);

    verify(eventRepository).findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(infoEvent.taskId());
    verify(newDepthEvent, never()).fire(ImmutableDepthReached.of(infoEvent.taskId(), infoEvent.depth()));
  }

  private void fire(EngineInformationReceived event) {
    weld.event().select(EngineInformationReceived.class).fire(event);
  }

}
