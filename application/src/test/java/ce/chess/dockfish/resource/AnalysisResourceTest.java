package ce.chess.dockfish.resource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ce.chess.dockfish.application.EvaluationMessageService;
import ce.chess.dockfish.application.InfiniteAnalysisService;
import ce.chess.dockfish.application.command.SubmitTaskCommand;
import ce.chess.dockfish.domain.model.result.AnalysisTime;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.EvaluationMessage;
import ce.chess.dockfish.domain.model.result.ImmutableEvaluation;
import ce.chess.dockfish.domain.model.result.ImmutableEvaluationMessage;
import ce.chess.dockfish.domain.model.result.ImmutableUciState;
import ce.chess.dockfish.domain.model.result.ImmutableVariation;
import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.result.ResultGame;
import ce.chess.dockfish.domain.model.result.Score;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableEngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.infrastructure.engine.EngineDirectoryConfiguration;

import com.google.common.io.Resources;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

@QuarkusTest
class AnalysisResourceTest {

  private static final String BASE_URI = "/api/tasks";
  private static final String POST_URI = BASE_URI;
  private static final String GET_URI = BASE_URI;
  private static final TaskId taskId = ImmutableTaskId.of("taskId");
  private static final Evaluation evaluation = ImmutableEvaluation.of(taskId, LocalDateTime.now(ZoneId.systemDefault()),
      Collections.singletonList(
          ImmutableVariation.of(1, "move", Score.fromCentiPawns(-42), 25, AnalysisTime.fromMilliSeconds(2222), "pgn")),
      ImmutableUciState.of(100, 10, 1, Set.of("some infoString")));
  private static final EvaluationMessage evaluationMessage = ImmutableEvaluationMessage.builder()
      .taskName("taskName")
      .analysedPgn("analysedPgn")
      .analysedFen("analysedFen")
      .uciEngineName("uciEngineName")
      .hostname("testhost")
      .status(JobStatus.NOT_ACTIVE)
      .evaluation(evaluation)
      .lastAlive(LocalDateTime.now(ZoneId.systemDefault()))
      .taskStarted(LocalDateTime.now(ZoneId.systemDefault()))
      .build();

  private static final EngineAnalysisRequest engineAnalysisRequest = ImmutableEngineAnalysisRequest.builder()
      .taskId(taskId)
      .name("name")
      .created(LocalDateTime.now(ZoneId.systemDefault()))
      .startingPosition(ResultGame.fromPgn("1. e4*"))
      .startingMoveNumber(42)
      .engineProgramName("taskDetails.engineProgramName()")
      .hostname("testhost")
      .initialPv(3)
      .maxDepth(30)
      .build();

  @InjectMock
  private InfiniteAnalysisService analysisService;

  @InjectMock
  private EvaluationMessageService messageService;

  @InjectMock
  private EngineDirectoryConfiguration engineDirectoryConfiguration;

  private final ArgumentCaptor<SubmitTaskCommand> submitTaskCommandCaptor =
      ArgumentCaptor.forClass(SubmitTaskCommand.class);

  @BeforeEach
  void setUp() {
    when(analysisService.startAsync(any())).thenReturn(Optional.of(taskId));
    when(analysisService.getJobStatus(taskId)).thenReturn(JobStatus.NOT_ACTIVE).thenReturn(JobStatus.ACTIVE);
    when(analysisService.getTaskDetails(taskId)).thenReturn(engineAnalysisRequest);
  }

  @Test
  void listEngineReturnsResult() {
    when(engineDirectoryConfiguration.listEngineNames()).thenReturn(Set.of("engine1", "engine2"));

    given()
        .accept(ContentType.JSON)
        .when()
        .get("/api/engines")
        .then()
        .log().ifValidationFails()
        .statusCode(HttpStatus.SC_OK)
        .body("$", contains("engine1", "engine2"));
  }

  @Test
  void postTask_depthOrDurationMustBePresent() {
    String payload = "{\"name\": \"someName\", \"pgn\": \"value\", \"initialPv\": 3}";

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .body(payload.getBytes(StandardCharsets.UTF_8))
        .when()
        .post(POST_URI)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }

  @Test
  void postTask_acceptsValidMessageAndDelegatesToService() throws IOException {
    String payload = resourceToString(AnalysisResourceTest.class.getSimpleName() + ".json");

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .body(payload.getBytes(StandardCharsets.UTF_8))
        .when()
        .post(POST_URI)
        .then()
        .statusCode(HttpStatus.SC_ACCEPTED);

    verify(analysisService).startAsync(submitTaskCommandCaptor.capture());
    SubmitTaskCommand task = submitTaskCommandCaptor.getValue();
    assertThat(task.maxDuration().orElseThrow(AssertionError::new).toHours(), is(equalTo(5L)));
    assertThat(task.options(), hasSize(2));
    assertThat(task.dynamicPv().isPresent(), is(true));

  }

  @Test
  void getResult_returnsResult() {
    when(messageService.getLastEvaluationMessage(taskId)).thenReturn(Optional.of(evaluationMessage));

    given()
        .accept(ContentType.JSON)
        .when()
        .get(GET_URI + "/" + taskId.rawId())
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(messageService).getLastEvaluationMessage(taskId);
  }

  @Test
  void getResultCurrent_returnsResult() {
    when(messageService.getLastEvaluationMessage()).thenReturn(Optional.of(evaluationMessage));

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .when()
        .get(GET_URI + "/current")
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(messageService).getLastEvaluationMessage();
  }

  @Test
  void getAll_returnsResult() {
    when(messageService.getAllEvaluations(taskId)).thenReturn(Collections.singletonList(evaluation));

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .when()
        .get(GET_URI + "/" + taskId.rawId() + "/all")
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(messageService).getAllEvaluations(taskId);
  }

  @Test
  void getAllCurrent_returnsResult() {
    when(messageService.getAllEvaluations()).thenReturn(Collections.singletonList(evaluation));

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .when()
        .get(GET_URI + "/current/all")
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(messageService).getAllEvaluations();
  }

  @Test
  void getTaskList_returnsList() {
    when(messageService.getAllTaskIds())
        .thenReturn(List.of(ImmutableTaskId.of("task1"), ImmutableTaskId.of("task2")));
    when(analysisService.getJobStatus(any())).thenReturn(JobStatus.NOT_ACTIVE).thenReturn(JobStatus.ACTIVE);
    when(analysisService.getTaskDetails(any())).thenReturn(engineAnalysisRequest);

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .when()
        .get(GET_URI)
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(messageService).getAllTaskIds();
  }

  @Test
  void postStopTask_delegates() {
    when(messageService.getLastEvaluationMessage(taskId)).thenReturn(Optional.of(evaluationMessage));

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .when()
        .post(POST_URI + "/" + taskId.rawId() + "/stop")
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(analysisService).stopAnalysis();
  }

  @Test
  void getStopTask_delegates() {
    when(analysisService.stopAnalysis()).thenReturn(true);
    when(messageService.getLastEvaluationMessage()).thenReturn(Optional.of(evaluationMessage));

    given()
        .contentType(ContentType.JSON)
        .accept(ContentType.JSON)
        .when()
        .get(GET_URI + "/stop")
        .then()
        .statusCode(HttpStatus.SC_OK);

    verify(analysisService).stopAnalysis();
  }

  @SuppressWarnings("UnstableApiUsage")
  private static String resourceToString(String filename) throws IOException {
    URL url = Resources.getResource(filename);
    return Resources.toString(url, StandardCharsets.UTF_8);
  }

}
