package ce.chess.integration.model;

import java.time.LocalDateTime;

public class EngineInformation {
  public int multiPv;
  public String lineSan;
  public int score;
  public String time;
  public int depth;
  public LocalDateTime occurredOn;

}
