package ce.chess.integration.model;

public class Variation {
  public int pvId;
  public String moves;
  public String score;
  public int depth;
  public String time;
  public String pgn;
}
