Feature: Post analysis tasks

  Scenario: Post invalid structured task
    When I post the content from "EvaluationRequestInvalid.json" to "/api/tasks"
    Then I receive a response with status 400
    And the response contains the text "Failed to deserialize java.time.Duration"

  Scenario: Post incorrect task
    When I post the content from "EvaluationRequestInvalidEngine.json" to "/api/tasks"
    Then I receive a response with status 400
    And the response contains the text "Engine not found"

  Scenario: Run task for Stockfish
    Given I listen to messages in the queue "dockfish.evaluations"
    When I start an evaluation for "EvaluationRequestStockfish.json"
    Then I will find this task in the list of tasks
    Then I can get details for this task
    And the task has the status "ACTIVE"

    When I stop this task
    And I can get details for this task
    Then the task has the status "NOT_ACTIVE"
    And the json element history[0] is equal to /^d=.*/
    And the json element hostname is equal to localtest
    And I will eventually get a message on "dockfish.evaluations"
    And the message has an element "taskName" equal to "nameOfStockfishGame"


  Scenario: Run task for Komodo with given duration
    Given I listen to messages in the queue "dockfish.evaluations"

    When I submit a task for game "1. e4 e5 2. Nf3 Nc6 3. Bb5" with 3 variations and the name "for komodo" and the duration "PT5S" to engine "komodo"
    Then I will find this task in the list of tasks

    Then I will eventually get a message on "dockfish.evaluations"
    And the message has an element "status" equal to "NOT_ACTIVE"
    And the message has an element "taskName" equal to "for komodo"


  Scenario: Post task with expected outcome and check json content
    When I post the content from "EvaluationRequestStockfishMateIn2.json" to "/api/tasks"
    Then I receive a response with status 202

    When I wait for 2 seconds
    And I stop all tasks
    Then I receive a response with status 200

    When I get "/api/tasks/current" from service
    Then I receive a response with status 200
    And the response body has the json content of "EvaluationMessageStockfishMateIn2.json"

